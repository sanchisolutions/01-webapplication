﻿using SanchiCommerce.Core;
using System;
using System.Configuration;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;


public partial class Modules_SanchiCommerce_PayUMoney_PayThroughPayUMoney : System.Web.UI.Page
{

    public string aspxPaymentModulePath, userName, cultureName, MainCurrency, Spath, itemIds, couponCode;
    public string sessionCode = string.Empty, SelectedCurrency = string.Empty;
    public int storeID, portalID, customerID;
    public double rate = 1;
    public string action1 = string.Empty;
    public string keyy;
    public string hash1 = string.Empty;
    public string txnid1 = string.Empty;
    public string PathPayU = string.Empty, couponApplied = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            keyy = ConfigurationManager.AppSettings["MERCHANT_KEY"];
            if (CheckOutSessions.Get("EBSData", "") != "")
            {
                string[] data = CheckOutSessions.Get("EBSData", "").Split('#');
                storeID = int.Parse(data[0].ToString());
                portalID = int.Parse(data[1].ToString());
                userName = data[2];
                customerID = int.Parse(data[3].ToString());
                sessionCode = data[4].ToString();
                cultureName = data[5];
                itemIds = data[6];
                couponCode = data[7];
                Spath = ResolveUrl("~/Modules/AspxCommerce/AspxCommerceServices/");
                StoreSettingConfig ssc = new StoreSettingConfig();
                MainCurrency = ssc.GetStoreSettingsByKey(StoreSetting.MainCurrency, storeID, portalID, cultureName);
                AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
                aspxCommonObj.StoreID = storeID;
                aspxCommonObj.PortalID = portalID;
                SelectedCurrency = "INR";
                if (rate != 0)
                {
                    // Set Transaction Currency code in session to save transactionLog table
                    Session["SelectedCurrency"] = SelectedCurrency;
                    LoadSetting();
                }
                //else
                //{
                //    lblnotity.Text = "Something goes wrong, hit refresh or go back to checkout";
                //    clickhere.Visible = false;
                //}
            }
            //else
            //{
            //    lblnotity.Text = "Something goes wrong, hit refresh or go back to checkout";
            //    clickhere.Visible = false;
            //}
        }
        catch (Exception ex)
        {
            //lblnotity.Text = "Something goes wrong, hit refresh or go back to checkout";
            //clickhere.Visible = false;
            ProcessException(ex);
        }
    }
    private void ProcessException(Exception ex)
    {
        throw new NotImplementedException();
    }
    [WebMethod]
    public static void SetSessionVariable(string key, string value)
    {
        HttpContext.Current.Session[key] = value;
    }
    public void LoadSetting()
    {
        //MoneybookersWCFService pw = new MoneybookersWCFService();
        // List<MoneybookersSettingInfo> sf;
        OrderDetailsCollection orderdata2 = new OrderDetailsCollection();
        orderdata2 = (OrderDetailsCollection)HttpContext.Current.Session["OrderCollection"];
        string itemidsWithVar = "";
        foreach (var item in orderdata2.LstOrderItemsInfo)
        {
            itemidsWithVar += item.ItemID + "#" + item.Quantity + "#" + orderdata2.ObjOrderDetails.OrderID + "#" + item.Variants + ",";
        }
        double amountTotal = CheckOutSessions.Get<double>("GrandTotalAll", 0) * rate;
        decimal amount = decimal.Parse(amountTotal.ToString(CultureInfo.InvariantCulture));
        string postURL = string.Empty;
        try
        {
            string[] hashVarsSeq;
            string hash_string = string.Empty;
            if (string.IsNullOrEmpty(Request.Form["txnid"])) // generating txnid
            {
                //Random rnd = new Random();
                //string strHash = Generatehash512(rnd.ToString() + DateTime.Now);
                //txnid1 = strHash.ToString().Substring(0, 20);
                txnid1 = Session["OrderID"].ToString();
            }
            else
            {
                //txnid1 = Request.Form["txnid"];
                txnid1 = Session["OrderID"].ToString();
            }
            if (string.IsNullOrEmpty(Request.Form["hash"])) // generating hash value
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MERCHANT_KEY"]) || !string.IsNullOrEmpty(txnid1))
                {
                    //frmError.Visible = false;
                    hashVarsSeq = ConfigurationManager.AppSettings["hashSequence"].Split('|'); // spliting hash sequence from config
                    hash_string = "";
                    foreach (string hash_var in hashVarsSeq)
                    {
                        if (hash_var == "key")
                        {
                            hash_string = hash_string + ConfigurationManager.AppSettings["MERCHANT_KEY"];
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "txnid")
                        {
                            hash_string = hash_string + txnid1;
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "amount")
                        {
                            hash_string = hash_string + amount;// Convert.ToDecimal(Request.Form[hash_var]).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "productinfo")
                        {
                            hash_string = hash_string + txnid1;// Convert.ToDecimal(Request.Form[hash_var]).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "firstname")
                        {
                            hash_string = hash_string + orderdata2.ObjBillingAddressInfo.FirstName;// Convert.ToDecimal(Request.Form[hash_var]).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else if (hash_var == "email")
                        {
                            hash_string = hash_string + orderdata2.ObjBillingAddressInfo.EmailAddress;// Convert.ToDecimal(Request.Form[hash_var]).ToString("g29");
                            hash_string = hash_string + '|';
                        }
                        else
                        {
                            hash_string = hash_string + (Request.Form[hash_var] != null ? Request.Form[hash_var] : "");// isset if else
                            hash_string = hash_string + '|';
                        }
                    }
                    hash_string += ConfigurationManager.AppSettings["SALT"];// appending SALT
                    hash1 = Generatehash512(hash_string).ToLower();         //generating hash
                    action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";// setting URL
                }
            }
            else if (!string.IsNullOrEmpty(Request.Form["hash"]))
            {
                hash1 = Request.Form["hash"];
                action1 = ConfigurationManager.AppSettings["PAYU_BASE_URL"] + "/_payment";
            }
            if (!string.IsNullOrEmpty(hash1))
            {
                //hash.Value = hash1;
                //txnid.Value = txnid1;
                System.Collections.Hashtable data = new System.Collections.Hashtable(); // adding values in gash table for data post
                data.Add("hash", hash1);
                data.Add("txnid", txnid1);
                data.Add("key", keyy);
                data.Add("abc", hash_string);
                string AmountForm = amount.ToString();// eliminating trailing zeros            
                //amount.Text = AmountForm;
                data.Add("amount", AmountForm);
                data.Add("firstname", orderdata2.ObjBillingAddressInfo.FirstName);
                data.Add("email", orderdata2.ObjBillingAddressInfo.EmailAddress);
                data.Add("phone", orderdata2.ObjBillingAddressInfo.Phone);
                data.Add("productinfo", txnid1);
                data.Add("surl", "http://www.kiranababu.com/PayUMoney-Success.aspx");
                data.Add("furl", "http://www.kiranababu.com");
                data.Add("lastname", orderdata2.ObjBillingAddressInfo.LastName);
                data.Add("curl", "");
                data.Add("address1", orderdata2.ObjShippingAddressInfo.Address);
                data.Add("address2", orderdata2.ObjShippingAddressInfo.Address);
                data.Add("city", orderdata2.ObjShippingAddressInfo.City);
                data.Add("state", orderdata2.ObjShippingAddressInfo.State);
                data.Add("country", orderdata2.ObjShippingAddressInfo.Country);
                data.Add("zipcode", orderdata2.ObjShippingAddressInfo.Zip);
                data.Add("udf1","");
                data.Add("udf2", "");
                data.Add("udf3","");
                data.Add("udf4", "");
                data.Add("udf5", "");
                data.Add("pg", "");
                data.Add("service_provider", "payu_paisa");
                string strForm = PreparePOSTForm(action1, data);
                Page.Controls.Add(new LiteralControl(strForm));
            }
            else
            {
                //no hash
            }
        }
        catch (Exception ex)
        {
            Response.Write("<span style='color:red'>" + ex.Message + "</span>");
        }
    }
    public string Generatehash512(string text)
    {
        byte[] message = Encoding.UTF8.GetBytes(text);
        UnicodeEncoding UE = new UnicodeEncoding();
        byte[] hashValue;
        SHA512Managed hashString = new SHA512Managed();
        string hex = "";
        hashValue = hashString.ComputeHash(message);
        foreach (byte x in hashValue)
        {
            hex += String.Format("{0:x2}", x);
        }
        return hex;
    }
    protected void clickhere_Click(object sender, EventArgs e)
    {
        LoadSetting();
    }
    private string PreparePOSTForm(string url, System.Collections.Hashtable data)// post form
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");
        foreach (System.Collections.DictionaryEntry keys in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + keys.Key + "\" value=\"" + keys.Value + "\">");
        }
        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." + formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }
}