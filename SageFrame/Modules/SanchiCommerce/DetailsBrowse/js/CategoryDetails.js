﻿//<![CDATA[
var categoryDetails = '';
var cat = '';
var rowTotal = 0;
var count = 0;
var templateScriptArr = [];
var NewItemArray = [];
var arrPrice = [];
var arry = new Array();
var variantId = new Array();
var brandIds = '';
var priceFrom;
var priceTo;
var Imagelist = '';
var arrCombination = [];
var SKU = '';
var ItemId = '';


IsExistedCategory = function (arr, cat) {
    var isExist = false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == cat) {
            isExist = true;
            break;
        }
    }
    return isExist;
};
CheckContains = function (checkon, toCheck) {
    var x = checkon.split('@');
    for (var i = 0; i < x.length; i++) {
        if (x[i] == toCheck) {
            return true;
        }
    }
    return false;
};
function getObjects(obj, key) {

    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects.push(obj[i][key]);
        }

    }
    return objects;
};
IsExists = function (arr, val) {
    var isExist = false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == val) {
            isExist = true; break;
        }
    }
    return isExist;
};
function getValByObjects(obj, key, key2, x, y, keyOfValue) {

    var value;
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            if (obj[i][key] == x && obj[i][key2] == y) {
                value = obj[i][keyOfValue];
            }
        }

    }
    return value;
};
indexOfArray = function (arr, val) {
    var arrIndex = -1;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == val) {
            arrIndex = i; break;
        }
    }
    return arrIndex;
};
BindCurrencySymbol = function () {
    var cookieCurrency = Currency.cookie.read();
    Currency.currentCurrency = BaseCurrency;
    Currency.format = 'money_format';
    Currency.convertAll(Currency.currentCurrency, cookieCurrency);

};

CheckVariantCombination = function (costVIds, costValIds, currentCostVar, currentCostVal, ItemId) {
    //debugger;
    categoryDetails.info.IsCombinationMatched = false;
    var Combination = [];
    $.each(arrCombination, function (index, item) {

        if (ItemId == item.ItemID) {

            Combination.push(item);
        }

    });
    var cvcombinationList = getObjects(Combination, 'CombinationType');
    var cvValuecombinationList = getObjects(Combination, 'CombinationValues');
    for (var j = 0; j < cvcombinationList.length; j++) {
        if (categoryDetails.info.IsCombinationMatched == true)
            break;
        var matchedIndex = 0;
        var matchedValues = 0;
        if (cvcombinationList[j].length == costVIds.length) {
            var cb = costVIds.split('@');
            for (var id = 0; id < cb.length; id++) {
                if (categoryDetails.info.IsCombinationMatched == true)
                    break;
                var element = cb[id];
                if (CheckContains(cvcombinationList[j], element)) {
                    matchedIndex++;
                    if (matchedIndex == cb.length) {
                        var cvb = costValIds.split('@');
                        for (var d = 0; d < cvb.length; d++) {
                            var element1 = cvb[d];
                            if (CheckContains(cvValuecombinationList[j], element1)) {
                                matchedValues++;
                            }
                            if (matchedValues == cvb.length) {
                                var combinationId = getValByObjects(Combination, 'CombinationType', 'CombinationValues', cvcombinationList[j], cvValuecombinationList[j], 'CombinationID');
                                var modifiers = getModifiersByObjects(Combination, combinationId);
                                categoryDetails.info.IsCombinationMatched = true;
                                categoryDetails.info.CombinationID = combinationId;
                                categoryDetails.info.IsPricePercentage = modifiers.IsPricePercentage;
                                categoryDetails.info.PriceModifier = modifiers.Price;
                                categoryDetails.info.Quantity = modifiers.Quantity;
                                categoryDetails.info.IsWeightPercentage = modifiers.IsWeightPercentage;
                                categoryDetails.info.WeightModifier = modifiers.Weight;
                                break;
                            } else {
                                categoryDetails.info.IsCombinationMatched = false;
                                categoryDetails.info.CombinationID = 0;
                                categoryDetails.info.IsPricePercentage = false;
                                categoryDetails.info.PriceModifier = 0;
                                categoryDetails.info.Quantity = 0;
                                categoryDetails.info.IsWeightPercentage = false;
                                categoryDetails.info.WeightModifier = 0;
                            }
                        }
                    } else {
                        categoryDetails.info.IsCombinationMatched = false;
                        categoryDetails.info.CombinationID = 0;
                        categoryDetails.info.IsPricePercentage = false;
                        categoryDetails.info.PriceModifier = 0;
                        categoryDetails.info.Quantity = 0;
                        categoryDetails.info.IsWeightPercentage = false;
                        categoryDetails.info.WeightModifier = 0;

                        var combinationIndex0 = cvcombinationList[j].split('@');
                        var combinationIndex01 = cvValuecombinationList[j].split('@');
                        for (var w = 0; w < combinationIndex0.length; w++) {
                            if (combinationIndex0[w] == currentCostVar) {
                                if (combinationIndex01[w] == currentCostVal) {

                                    if (!IsExists(categoryDetails.info.AvailableCombination, cvValuecombinationList[j]))
                                        categoryDetails.info.AvailableCombination.push(cvValuecombinationList[j]);
                                }
                            }
                        }
                    }
                }

            }
        }
        else {
            var combinationIndex = cvcombinationList[j].split('@');
            var combinationIndex1 = cvValuecombinationList[j].split('@');
            for (var z = 0; z < combinationIndex.length; z++) {
                if (combinationIndex[z] == currentCostVar) {
                    if (combinationIndex1[z] == currentCostVal) {
                        if (!IsExists(categoryDetails.info.AvailableCombination, cvValuecombinationList[j]))
                            categoryDetails.info.AvailableCombination.push(cvValuecombinationList[j]);
                    }
                }
            }
        }
    }
    return categoryDetails.info;
};
$(function () {
    var aspxCommonObj = {
        StoreID: SanchiCommerce.utils.GetStoreID(),
        PortalID: SanchiCommerce.utils.GetPortalID(),
        UserName: SanchiCommerce.utils.GetUserName(),
        CultureName: SanchiCommerce.utils.GetCultureName(),
        CustomerID: SanchiCommerce.utils.GetCustomerID(),
        SessionCode: SanchiCommerce.utils.GetSessionCode()
    };
    var templatePath = SanchiCommerce.utils.GetAspxTemplateFolderPath();

    var arrItemsOption = new Array();
    //var arrItemsOptionToBind = new Array();//added by jyoti
    var currentpage = 0;
    var isByCategory = false;
    var isFirstCall = true;
    categoryDetails = {
        config: {
            isPostBack: false,
            async: true,
            cache: true,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: '{}',
            dataType: 'json',
            baseURL: aspxservicePath + "AspxCoreHandler.ashx/",
            method: "",
            url: "",
            ajaxCallMode: "",
            itemid: 0,
            oncomplete: 0
        },
        BindCostVariantCombination: function (msg) {

            $.each(msg.d, function (index, item) {
                var CostVariantCombination = {
                    CombinationID: 0,
                    CombinationType: "",
                    CombinationValues: "",
                    CombinationPriceModifier: "",
                    CombinationPriceModifierType: "",
                    CombinationWeightModifier: "",
                    CombinationWeightModifierType: "",
                    CombinationQuantity: 0,
                    ImageFile: "",
                    ItemID: ""
                };
                CostVariantCombination = item;
                arrCombination.push(CostVariantCombination);
            });
            // selectFirstcombination(ItemId);
        },
        oncomplete: function () {

            switch (ItemDetail.config.oncomplete) {
                case 20:
                    ItemDetail.config.oncomplete = 0;
                    if ($("#divCartDetails").length > 0) {
                        AspxCart.GetUserCartDetails();
                    }
                    if ($("#dynItemDetailsForm").length > 0) {
                        ItemDetail.BindItemBasicByitemSKU(itemSKU);
                    }
                    break;
            }
        },
        GetAddToCartErrorMsg: function () {

            csscody.error('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Failed to add item to cart!') + '</p>');
        },
        AddItemstoCartFromDetail: function (msg) {

            if (msg.d == 1) {
                var myCartUrl;
                myCartUrl = myCartURL + pageExtension;
                var addToCartProperties = {
                    onComplete: function (e) {
                        if (e) {
                            window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                        }
                    }
                };
                csscody.addToCart('<h2>' + getLocale(DetailsBrowse, "Successful Message") + '</h2><p>' + getLocale(DetailsBrowse, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);
                if (allowRealTimeNotifications.toLowerCase() == 'true') {
                    try {
                        var itemOnCart = $.connection._aspxrthub;
                        itemOnCart.server.checkIfItemOutOfStock(itemId, itemSKU, "", SanchiCommerce.AspxCommonObj());

                    }
                    catch (Exception) {
                        console.log(getLocale(DetailsBrowse, 'Error Connecting Hub.'));
                    }
                }
                HeaderControl.GetCartItemTotalCount(); ShopingBag.GetCartItemCount(); ShopingBag.GetCartItemListDetails();
            }
            else if (msg.d == 2) {
                if (allowOutStockPurchase.toLowerCase() == 'false') {
                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(AspxItemDetailsBrowseDetails, 'This product is currently Out Of Stock!') + "</p>");
                }
                else {
                    var myCartUrl = myCartURL + pageExtension;
                    var addToCartProperties = {
                        onComplete: function (e) {
                            if (e) {
                                window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                            }
                        }
                    };
                    csscody.addToCart('<h2>' + getLocale(DetailsBrowse, "Successful Message") + '</h2><p>' + getLocale(DetailsBrowse, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);
                    HeaderControl.GetCartItemTotalCount();
                    ShopingBag.GetCartItemCount();
                    ShopingBag.GetCartItemListDetails();
                }
            }
        },
        GetTemplate: function (key) {
            var val = "";
            if (templateScriptArr.length > 0) {
                for (var i = 0; i < templateScriptArr.length; i++) {
                    if (templateScriptArr[i].TemplateKey == key) {
                        val = templateScriptArr[i].TemplateValue;
                        break;
                    }
                }
            }
            return val;
        },
        AddToMyCart: function (itemTypeId, ItmID) {
            //debugger;
            if (itemTypeId == 3) {
                var giftCardDetail = {
                    Price: $("#hdnPrice_" + ItemId + "").val(),
                    GiftCardTypeId: parseFloat($("input[name=giftcard-type]:checked").val()),
                    GiftCardCode: '',
                    GraphicThemeId: parseFloat($(".jcarousel-skin ul li a.selected").attr('data-id')),
                    SenderName: $.trim($("#txtgc_senerName").val()),
                    SenderEmail: $.trim($("#txtgc_senerEmail").val()),
                    RecipientName: $.trim($("#txtgc_recieverName").val()),
                    RecipientEmail: $.trim($("#txtgc_recieverEmail").val()),
                    Messege: $.trim($("#txtgc_messege").val())
                };


                if (parseFloat($("input[name=giftcard-type]:checked").val()) == 2) {

                } else {
                    if ($.trim($("#txtgc_recieverName").val()) == "" ||
                        $.trim($("#txtgc_recieverEmail").val()) == "") {

                        csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Please fill valid required data!') + '</p>');
                        return false;

                    } else {
                        if (!/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(giftCardDetail.RecipientEmail)) {
                            csscody.alert("<h2>" + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid email address!") + "</p>");
                            return false;
                        }

                    }
                }

                if (giftCardDetail.SenderName != "" || giftCardDetail.SenderEmail != "") {
                    if (!/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(giftCardDetail.SenderEmail)) {
                        csscody.alert("<h2>" + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid email address!") + "</p>");
                        return false;
                    }
                    var AddItemToCartObj = {
                        ItemID: itemId,
                        Price: $("#hdnPrice").val(),
                        Weight: 0,
                        Quantity: 1,
                        CostVariantIDs: '0@',
                        IsGiftCard: true,
                        IsKitItem: false
                    };
                    var paramz = {
                        aspxCommonObj: aspxCommonObj(),
                        AddItemToCartObj: AddItemToCartObj,
                        giftCardDetail: giftCardDetail,
                        kitInfo: {}
                    };
                    var dataz = JSON2.stringify(paramz);
                    this.config.method = "AspxCommonHandler.ashx/AddItemstoCartFromDetail";
                    this.config.url = this.config.baseURL + this.config.method;
                    this.config.data = dataz;
                    this.config.ajaxCallMode = categoryDetails.AddItemstoCartFromDetail;
                    this.config.oncomplete = 20;
                    this.config.error = categoryDetails.GetAddToCartErrorMsg;
                    this.ajaxCall(this.config);
                }
                else {
                    csscody.alert('<h2>' + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid required data!") + "</p>");
                    return false;
                }
            }
            else if (itemTypeId == 6) {

                var kitinfo = categoryDetails.Kit.GetKitInfo();
                if (kitinfo.Data.length == 0) {
                    csscody.alert('<h2>' + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please choose valid configuration!") + "</p>");
                    return false;
                }
                var AddItemToCartObj = {
                    ItemID: itemId,
                    Price: kitinfo.Price,
                    Weight: kitinfo.Weight,
                    Quantity: $.trim($("#txtQty_" + itemId + "").val()),
                    CostVariantIDs: '0@',
                    IsGiftCard: false,
                    IsKitItem: true
                };
                var paramz = {
                    aspxCommonObj: aspxCommonObj(),
                    AddItemToCartObj: AddItemToCartObj,
                    giftCardDetail: {},
                    kitInfo: kitinfo
                };
                var itemsCartInfo = categoryDetails.CheckItemQuantityInCart(itemId, '0@');
                var itemQuantityInCart = itemsCartInfo.ItemQuantityInCart;
                var userItemQuantityInCart = itemsCartInfo.UserItemQuantityInCart;

                if (itemQuantityInCart != -1) {
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {
                            if ((eval($("#txtQty_" + itemId + "").val()) + eval(userItemQuantityInCart)) < eval(categoryDetails.info.MinCartQuantity)) {
                                csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(AspxItemDeDetailsBrowsetails, 'The requested quantity for this item is not valid') + "</p>");
                                return false;
                            }
                            else if ((eval($("#txtQty_" + itemId + "").val()) + eval(userItemQuantityInCart)) > eval(categoryDetails.info.MaxCartQuantity)) {
                                csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                return false;
                            }
                            else if (categoryDetails.info.Quantity <= 0) {
                                csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                return false;
                            } else {
                                if ((eval($.trim($("#txtQty_" + itemId + "").val())) + eval(itemQuantityInCart)) > eval(categoryDetails.info.Quantity)) {
                                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                    return false;
                                }
                            }
                        }
                    }
                    else {
                        $("#btnAddToMyCart").parent('label').remove();
                    }
                }
                var dataz = JSON2.stringify(paramz);
                this.config.method = "AspxCommonHandler.ashx/AddItemstoCartFromDetail";
                this.config.url = this.config.baseURL + this.config.method;
                this.config.data = dataz;
                this.config.ajaxCallMode = categoryDetails.AddItemstoCartFromDetail;
                this.config.oncomplete = 20;
                this.config.error = categoryDetails.GetAddToCartErrorMsg;
                this.ajaxCall(this.config);
            }
            else {
                checkAvailibility($("#divCostVariant_" + ItmID + " select"));
                if (categoryDetails.info.IsCombinationMatched) {

                    if ($.trim($("#txtQty_" + ItmID + "").val()) == "" || $.trim($("#txtQty_" + ItmID + "").val()) <= 0) {
                        csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Invalid quantity.') + '</p>');
                        return false;
                    }
                    var itemPrice = $("#hdnPrice_" + ItmID + "").val();
                    var itemQuantity = $.trim($("#txtQty_" + ItmID + "").val());
                    var Qty = $("#hdnQuantity_" + ItmID + "").val();
                    var itemCostVariantIDs = [];
                    var weightWithVariant = 0;
                    var totalWeightVariant = 0;
                    var costVariantPrice = 0;

                    if (!$("#divCostVariant_" + ItmID + "").html().trim()) {
                        itemCostVariantIDs.push(0);
                    } else {
                        $("#divCostVariant_" + ItmID + " select option:selected").each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs.push($(this).val());
                            } else {
                            }
                        });
                        $("#divCostVariant_" + ItmID + " input[type=radio]:checked").each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs.push($(this).val());
                            } else {
                            }
                        });

                        $("#divCostVariant_" + ItmID + " input[type=checkbox]:checked").each(function () {
                            if ($(this).val() != 0) {
                                itemCostVariantIDs.push($(this).val());
                            } else { }
                        });
                    }
                    //debugger;
                    var itemsCartInfo = categoryDetails.CheckItemQuantityInCart(ItmID, itemCostVariantIDs.join('@') + '@');
                    var itemQuantityInCart = itemsCartInfo.ItemQuantityInCart;
                    var userItemQuantityInCart = itemsCartInfo.UserItemQuantityInCart;

                    if (itemQuantityInCart != -1) {
                        if (allowAddToCart.toLowerCase() == 'true') {
                            if (allowOutStockPurchase.toLowerCase() == 'false') {
                                //if ((eval($("#txtQty_" + ItmID + "").val()) + eval(userItemQuantityInCart)) < eval(categoryDetails.info.MinCartQuantity)) {
                                //    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                //    return false;
                                //}
                                //else if ((eval($("#txtQty_" + ItmID + "").val()) + eval(userItemQuantityInCart)) > eval(categoryDetails.info.MaxCartQuantity)) {
                                //    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                //    return false;
                                //}
                                //else 
                                if (categoryDetails.info.Quantity <= 0 && Qty <= 0) {
                                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                    return false;
                                } else {
                                    if ((eval($.trim($("#txtQty_" + ItmID + "").val())) + eval(itemQuantityInCart)) > eval(categoryDetails.info.Quantity) && (eval($.trim($("#txtQty_" + ItmID + "").val())) + eval(itemQuantityInCart)) > eval(Qty)) {
                                        csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                        return false;
                                    }
                                }
                            }
                        }
                        else {
                            $("#btnAddToMyCart_" + ItmID + "").parent('label').remove();
                        }
                    }
                    if (categoryDetails.info.IsPricePercentage) {
                        costVariantPrice = eval($("#hdnPrice_" + ItmID + "").val()) * eval(categoryDetails.info.PriceModifier) / 100;
                    } else {
                        costVariantPrice = eval(categoryDetails.info.PriceModifier);
                    }
                    if (categoryDetails.info.IsWeightPercentage) {
                        weightWithVariant = eval($("#hdnWeight_" + ItmID + "").val()) * eval(categoryDetails.info.WeightModifier) / 100;
                    } else {
                        weightWithVariant = eval(categoryDetails.info.WeightModifier);
                    }

                    totalWeightVariant = eval($("#hdnWeight_" + ItmID + "").val()) + eval(weightWithVariant);
                    itemPrice = eval(itemPrice) + eval(costVariantPrice);
                    var AddItemToCartObj = {
                        ItemID: ItmID,
                        Price: itemPrice,
                        Weight: totalWeightVariant,
                        Quantity: itemQuantity,
                        CostVariantIDs: itemCostVariantIDs.join('@') + '@',
                        IsGiftCard: false,
                        IsKitItem: false
                    };
                    var paramz = {
                        aspxCommonObj: aspxCommonObj,
                        AddItemToCartObj: AddItemToCartObj,
                        giftCardDetail: {},
                        kitInfo: {}
                    };
                    var data = JSON2.stringify(paramz);
                    this.config.method = "AddItemstoCartFromDetail";
                    this.config.url = this.config.baseURL + this.config.method;
                    this.config.data = data;
                    this.config.ajaxCallMode = categoryDetails.AddItemstoCartFromDetail;
                    this.config.oncomplete = 20;
                    this.config.error = categoryDetails.GetAddToCartErrorMsg;
                    this.ajaxCall(this.config);
                } else {
                    csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Please choose available variants!') + '</p>');
                }
            }

        },
        vars: {
            countCompareItems: "",
            itemSKU: SKU,
            itemQuantityInCart: "",
            userItemQuantityInCart: "",
            //userEmail: userEmail,
            //itemId: itemID,
            existReviewByUser: "",
            existReviewByIP: ""
        },
        info: {
            IsCombinationMatched: false,
            AvailableCombination: [],
            CombinationID: 0,
            PriceModifier: '',
            IsPricePercentage: false,
            WeightModifier: '',
            IsWeightPercentage: false,
            Quantity: 0,
            MinCartQuantity: 0,
            MaxCartQuantity: 0

        },

        init: function () {

            $.each(jsTemplateArray, function (index, value) {
                var tempVal = jsTemplateArray[index].split('@');
                var templateScript = {
                    TemplateKey: tempVal[0],
                    TemplateValue: tempVal[1]
                };
                templateScriptArr.push(templateScript);
            });
            $(":checkbox").prop("checked", false);
            categoryDetails.BindShoppingFilterResult();
            // categoryDetails.LoadAllCategoryContents();

            if (isCategoryHasItems != undefined && isCategoryHasItems != 0) {

                $("#divShopFilter").show();
                $("#tblFilter").show();
            }
            else {
                $("#divShopFilter").hide();
                $("#tblFilter").hide();
                $("#divShowCategoryItemsList").html("<span class=\"cssClassNotFound\">" + getLocale(DetailsBrowse, "No items found!") + "</span>");
            }
            cat = categorykey;

            $("#slider-range").slider({
                range: true,
                min: minPrice,
                max: maxPrice,
                step: 0.001,
                values: [minPrice, maxPrice],
                slide: function (event, ui) {
                   // debugger;
                    $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[0]).toFixed(2) + "</span>" + " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[1]).toFixed(2) + "</span>");
                    BindCurrencySymbol();
                },
                change: function (event, ui) {
                    // debugger;
                    if (isFirstCall == true) {
                        $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[0]).toFixed(2) + "</span>" + " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[1]).toFixed(2) + "</span>");
                        BindCurrencySymbol();
                        if (event.originalEvent == undefined && count == 0) {
                            categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
                            count = count + 1;
                        }
                        else if (event.originalEvent != undefined) {
                            categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
                        }
                    }
                }
            });
            categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
            $("#tblFilter").show();
            $("#tblFilter").find('h2').html('<p>' + getLocale(DetailsBrowse, "Filters") + '</p>');
            $("#divShoppingFilter").hide();
            $("#divShoppingFilter").show();
            var brandArr = [];
            $(":checkbox").uniform();
            $("#ddlSortBy").change(function () {
                var items_per_page = $('#ddlPageSize').val();
                var offset = 1;
                categoryDetails.GetDetail(offset, items_per_page, 0, $("#ddlSortBy").val());
            });
            $("#ddlPageSize").change(function () {
                //
                var items_per_page = $(this).val();
                var offset = 1;
                categoryDetails.GetDetail(offset, items_per_page, 0, $("#ddlSortBy").val());
            });
            $("#ddlViewAs").change(function () {
                categoryDetails.BindShoppingFilterResult();

            });
            $("#divViewAs").on('click', "a", function () {
                $("#divViewAs").find('a').removeClass('sfactive');
                $(this).addClass("sfactive");
                categoryDetails.BindShoppingFilterResult();
            });
            $(".filter").on('click', ".chkCategory", function () {

                minPrice = 0;
                maxPrice = 0;
                isCategoryHasItems = 0;
                arrPrice = [];
                NewItemArray = [];
                brandIds = '';
                arry.length = 0;
                var values = $(this).attr('ids');
                var isChecked = false;
                $('.chkCategory').each(function () {
                    if ($(this).prop('checked') == true) {
                        isChecked = true;
                        return false;
                    }
                });
                if ($(this).prop('checked') == true) {
                    if (indexOfArray(brandArr, values) == -1) {
                        brandArr.push(values);
                    }
                }
                else {
                    if (indexOfArray(brandArr, values) != -1) {
                        brandArr.splice(indexOfArray(brandArr, values), 1);
                    }
                }
                if (isChecked) {
                    cat = brandArr.join(',');
                    isByCategory = true;
                }
                else {
                    cat = categorykey;
                    isByCategory = false;
                }
                $('.filter > div:gt(0)').remove();
                categoryDetails.GetAllBrandForCategory();
                categoryDetails.GetShoppingFilter();
                $(":checkbox").not(".chkCategory").uniform();
                if (isCategoryHasItems != 0) {

                    $("#divItemViewOptions").show();
                    $("#divSearchPageNumber").show();
                    var offset = 1;
                    categoryDetails.GetDetail(offset, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
                } else {
                    $("#divItemViewOptions").hide();
                    $("#divSearchPageNumber").hide();
                    $("#divShowCategoryItemsList").html("No items found!");
                }
            });

            $(".filter").on('click', ".chkFilter", function () {
                var isChecked = false;
                brandIds = '';
                $('.chkFilter').each(function () {
                    if ($(this).prop('checked') == true) {
                        isChecked = true;
                        return false;
                    }
                });
                if (!$(this).hasClass('chkBrand')) {
                    var attrValue = $(this).prop('name');
                    var attrIds = $(this).val();
                    var inputTypeId = $(this).attr('inputTypeID');
                    var values = attrIds + '@' + inputTypeId + '@' + attrValue;
                    if ($(this).prop('checked') == true) {
                        if (indexOfArray(arry, values) == -1) {
                            arry.push(values);
                        }
                    }

                    if ($(this).prop('checked') == false) {
                        if (indexOfArray(arry, values) != -1) {
                            arry.splice(indexOfArray(arry, values), 1);
                        }
                        if (indexOfArray(arry, values) == -1) {
                            $(this).parents('ul').find(".chkFilter").each(function () {
                                if ($(this).prop('checked') == true) {
                                    attrIds + '@' + inputTypeId + '@' + attrValue;
                                    var chkval = attrIds + '@' + $(this).attr('inputTypeID') + '@' + $(this).val();
                                    if (chkval == values) {
                                        if (indexOfArray(arry, values) == -1) {
                                            arry.push(values);
                                        }
                                    }
                                }
                            });
                        }
                    }
                    var aux = {};
                    $.each(arry, function (idx, val) {
                        var key = [];
                        key[0] = val.substring(0, val.lastIndexOf('@'));
                        key[1] = val.substring(val.lastIndexOf('@') + 1, val.length);
                        if (!aux[key[0]]) {
                            aux[key[0]] = [];
                        }
                        aux[key[0]].push(key[1]);
                    });

                    NewItemArray = [];
                    $.each(aux, function (idx, val) {
                        NewItemArray.push(idx + '@' + val.join("#"));
                    });
                }
                if ($(".divContent0").length > 0) {
                    $('.chkBrand').each(function () {
                        if ($(this).prop('checked') == true) {
                            brandIds += $(this).attr('ids') + ',';
                        }
                    });
                    brandIds = brandIds.substring(0, brandIds.lastIndexOf(','));
                }
                categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
            });

            $(".filter").on('click', 'div[value="8"] a', function () {
                count = 0;
                $("#slider-range").slider("option", "values", [$(this).attr('minprice'), $(this).attr('maxprice')]);
                $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat($("#slider-range").slider("values", 0)).toFixed(2) + "</span>" +
                " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat($("#slider-range").slider("values", 1)).toFixed(2) + "</span>");
                BindCurrencySymbol();
            });

            $(".filter").on('click', '.divTitle', function () {
                var imgPath = $(this).find('img').prop('src');

                if (imgPath.toString().indexOf("arrow_down.png") != -1) {
                    $(this).find('img').prop('src', '' + templatePath + '/images/arrow_up.png');

                } else {
                    $(this).find('img').prop('src', '' + templatePath + '/images/arrow_down.png');

                }
                if ($(this).parent().attr('value') == '8') {
                    $(".divContent" + $(this).parent().attr('value')).slideToggle('fast');
                    $(".divRange").slideToggle('fast');
                } else {
                    $(this).next(".cssClassScroll").slideToggle('fast');
                }
            });
            $(".filter").find('a').on('hover', 'div[value="8"] a', function () {
                $(this).css("text-decoration", "underline");
            });
            $(".filter").find('a').on('mouseout', 'div[value="8"] a', function () {
                $(this).css("text-decoration", "none");
            });
        },
        BindInsideControl: function (item, controlID) {
            var optionValues = '';
            var costPriceValue = item.CostVariantsPriceValue;
            var weightValue = item.CostVariantsWeightValue;
            if (item.InputTypeID == 5) {
                optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";

            } else if (item.InputTypeID == 6) {
                optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";

            }
            return optionValues;
        },
        ajaxCall: function (config) {
            $.ajax({
                type: categoryDetails.config.type,
                contentType: categoryDetails.config.contentType,
                cache: categoryDetails.config.cache,
                async: categoryDetails.config.async,
                url: categoryDetails.config.url,
                data: categoryDetails.config.data,
                dataType: categoryDetails.config.dataType,
                success: categoryDetails.config.ajaxCallMode,
                error: categoryDetails.ajaxFailure
            });
        },
        GetAllSubCategoryForFilter: function () {
            var attribute = $.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : null;//added by aniket for citywise
            var param = JSON2.stringify({ categorykey: cat, aspxCommonObj: aspxCommonObj, attribute: attribute });
            this.config.method = "GetAllSubCategoryForFilter";
            this.config.url = this.config.baseURL + this.config.method;
            this.config.data = param;
            this.config.ajaxCallMode = categoryDetails.BindSubCategoryForFilter;
            this.config.async = false;
            this.ajaxCall(this.config);
        },
        GetAllBrandForCategory: function () {
            var attribute = $.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : null;//added by aniket for citywise
            var param = JSON2.stringify({ categorykey: cat, isByCategory: isByCategory, aspxCommonObj: aspxCommonObj, attribute: attribute });
            this.config.method = "GetAllBrandForCategory";
            this.config.url = this.config.baseURL + this.config.method;
            this.config.data = param;
            this.config.ajaxCallMode = categoryDetails.BindBrandForCategory;
            this.config.async = false;
            this.ajaxCall(this.config);
        },
        GetShoppingFilter: function () {
            var attribute = $.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : null;//added by aniket for citywise
            var param = JSON2.stringify({ aspxCommonObj: aspxCommonObj, categoryName: cat, isByCategory: isByCategory, attribute: attribute });
            this.config.method = "GetShoppingFilter";
            this.config.url = this.config.baseURL + this.config.method;
            this.config.data = param;
            this.config.ajaxCallMode = categoryDetails.BindShoppingFilter;
            this.config.async = false;
            this.ajaxCall(this.config);
        },
        GetItemDetail: function (brandIds, attributes, limit, offset, sortBy) {
           // debugger;
            currentPage = currentpage;
            var param = JSON2.stringify({ brandIds: brandIds, attributes: attributes, priceFrom: priceFrom, priceTo: priceTo, categoryName: cat, isByCategory: isByCategory, limit: limit, offset: offset, sortBy: sortBy, aspxCommonObj: aspxCommonObj });
            this.config.method = "GetShoppingFilterItemsResult";
            this.config.url = this.config.baseURL + this.config.method;
            this.config.data = param;
            this.config.async = false;
            this.config.ajaxCallMode = categoryDetails.BindItemDetail;
            this.ajaxCall(this.config);
        },
        GetDetail: function (offset1, limit1, currentpage1, sortBy) {
        //    debugger;
            currentpage = currentpage1;
            sortBy1 = sortBy == undefined ? 5 : sortBy;
            limit1 = limit1 == undefined ? 9 : limit1;
            priceFrom = minPrice;//$("#slider-range").slider("values", 0);
            priceTo = maxPrice;//$("#slider-range").slider("values", 1);
            //$.cookie("cityname")
            NewItemArray.push($.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : null);//added by aniket for get items citywise
            categoryDetails.GetItemDetail(brandIds, NewItemArray.join(','), limit1, offset1, sortBy1);
            categoryDetails.ResizeImageDynamically(Imagelist);
            categoryDetails.BindShoppingFilterResult();
        },
        LoadCostVariantCombination: function (itemSKU) {
            var param = JSON2.stringify({ itemSku: itemSKU, aspxCommonObj: aspxCommonObj });
            this.config.method = "AspxCoreHandler.ashx/GetCostVariantCombinationbyItemSku";
            this.config.url = this.config.baseURL + this.config.method;
            this.config.data = param;
            this.config.ajaxCallMode = categoryDetails.BindCostVariantCombination;
            this.config.async = false;
            this.ajaxCall(this.config);
        },
        CreateControl: function (item, controlID, isChecked) {
            var controlElement = '';
            var costPriceValue = item.CostVariantsPriceValue;
            var weightValue = item.CostVariantsWeightValue;
            if (item.InputTypeID == 5) {
                controlElement = "<select id='" + controlID + "' multiple></select>";
            } else if (item.InputTypeID == 6) {
                controlElement = "<select id='" + controlID + "'></select>";
            } else if (item.InputTypeID == 9 || item.InputTypeID == 10) {
                controlElement = "<label><input  name='" + controlID + "' type='radio' checked='checked' value='" + item.CostVariantsValueID + "'><span>" + item.CostVariantsValueName + "</span></label>";

            } else if (item.InputTypeID == 11 || item.InputTypeID == 12) {
                controlElement = "<input  name='" + controlID + "' type='radio' checked='checked' value='" + item.CostVariantsValueID + "'><label>" + item.CostVariantsValueName + "</label></br>";

            }
            return controlElement;
        },
        CheckItemQuantityInCart: function (ID, itemCostVariantIDs) {
            //debugger;
            categoryDetails.vars.itemQuantityInCart = 0;
            categoryDetails.vars.userItemQuantityInCart = 0;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                async: false,
                url: aspxservicePath + 'AspxCoreHandler.ashx/CheckItemQuantityInCart',
                data: JSON2.stringify({ itemID: ID, aspxCommonObj: aspxCommonObj, itemCostVariantIDs: itemCostVariantIDs }),
                dataType: "json",
                success: function (data) {
                    categoryDetails.vars.itemQuantityInCart = data.d.ItemQuantityInCart;
                    categoryDetails.vars.userItemQuantityInCart = data.d.UserItemQuantityInCart;
                },
                error: function () { }
            });
            var itemsCartInfo = {
                ItemQuantityInCart: categoryDetails.vars.itemQuantityInCart,
                UserItemQuantityInCart: categoryDetails.vars.userItemQuantityInCart
            };
            return itemsCartInfo;
        },
        BindCostVariant: function (data, itemSKU, ItemID) {
            SKU = itemSKU;
            ItemId = ItemID;
            if (data != null) {
                var CostVariant = '';
                var variantValue = [];
                $.each(data, function (index, item) {
                    if (index != "clean") {
                        var CostVariantID = data[index].split(',')[0];
                        var CostVariantName = data[index].split(',')[1];
                        var InputTypeID = data[index].split(',')[2];
                        var CostVariantsValueID = data[index].split(',')[3];
                        var CostVariantsValueName = data[index].split(',')[4];
                        if (CostVariant.indexOf(CostVariantID) == -1) {
                            CostVariant += CostVariantID;
                            variantId.push(CostVariantID);
                            var addSpan = '';
                            addSpan += '<div id="div_' + CostVariantID + '_' + ItemID + '" class="cssClassHalfColumn_' + ItemID + '">';
                            //addSpan += '<span id="spn_' + CostVariantID + '_'+ ItemID +'" ><b>' + CostVariantName + ':</b> ' + '</span>';
                            //addSpan += '<span class="spn_Close_'+ ItemID +'"><a href="#"><img class="imgDelete" src="' + aspxTemplateFolderPath + '/images/admin/uncheck.png" title="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"" alt="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"/></a></span>';
                            $('#divCostVariant_' + ItemID + '').append(addSpan);
                            addSpan += '</div>';
                        }
                        var items = {
                            CostVariantID: CostVariantID,
                            CostVariantName: CostVariantName,
                            CostVariantsPriceValue: null,
                            CostVariantsValueID: CostVariantsValueID,
                            CostVariantsValueName: CostVariantsValueName,
                            CostVariantsWeightValue: null,
                            InputTypeID: InputTypeID,
                            IsOutOfStock: false,
                            IsPriceInPercentage: null,
                            IsWeightInPercentage: null
                        }
                        var valueID = '';
                        var itemCostValueName = '';
                        if (CostVariantsValueID != -1) {
                            if (InputTypeID == 5 || InputTypeID == 6) {
                                if ($('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').length == 0) {
                                    itemCostValueName += '<span class="sfListmenu" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                    valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                    itemCostValueName += categoryDetails.CreateControl(items, valueID, false);
                                    itemCostValueName += "</span>";
                                    $('#div_' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                }
                                if (!IsExists(variantValue, CostVariantsValueID)) {
                                    variantValue.push(CostVariantsValueID);
                                    optionValues = categoryDetails.BindInsideControl(items, valueID);
                                    $('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').append(optionValues);
                                }
                                $('#controlCostVariant_' + CostVariantID + '_' + ItemId + ' option:first-child').prop("selected", "selected");
                            }
                            else {
                                if ($('#subDiv' + CostVariantID + '_' + ItemId + '').length == 0) {
                                    itemCostValueName += '<span class="cssClassRadio" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                    valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                    itemCostValueName += categoryDetails.CreateControl(items, valueID, true);
                                    itemCostValueName += "</span>";
                                    $('#div_' + CostVariantID + '').append(itemCostValueName);
                                } else {
                                    valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                    itemCostValueName += categoryDetails.CreateControl(items, valueID, false);
                                    $('#subDiv' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                }
                            }
                        }
                    }
                });
                $('#divCostVariant_' + ItemID + '').append('<div class="cssClassClear"></div>');
                if (!$('#divCostVariant_' + ItemID + '').html().trim()) {
                    $('#divCostVariant_' + ItemID + '').removeClass("cssClassCostVariant");
                } else {
                    $('#divCostVariant_' + ItemID + '').addClass("cssClassCostVariant");
                }
                if ($.session("ItemCostVariantData") != undefined) {
                    $.each(arrCostVariants, function (i, variant) {
                        var itemColl = $('#divCostVariant_' + ItemID + '').find("[Variantname=" + variant + "]");
                        if ($(itemColl).is("input[type='checkbox'] ,input[type='radio']")) {
                            $('#divCostVariant_' + ItemID + '').find("input:checkbox").removeAttr("checked");
                            $(itemColl).prop("checked", "checked");
                        } else if ($(itemColl).is('select>option')) {
                            $('#divCostVariant_' + ItemID + '').find("select>option").removeAttr("selected");
                            $(itemColl).prop("selected", "selected");
                        }
                    });
                    $.session("ItemCostVariantData", 'empty');
                }
                $('#divCostVariant_' + ItemID + ' select,#divCostVariant_' + ItemID + ' input[type=radio],#divCostVariant_' + ItemID + ' input[type=checkbox]').unbind().bind("change", function () {

                    checkAvailibility(this);
                });

                $('#divCostVariant_' + ItemID + '').on("click", ".spn_Close", function () {

                    $(this).next('span:first').find(" input[type=radio]").removeAttr('checked');
                    if ($(this).next('span:first').find("select").find("option[value=0]").length == 0) {
                        var options = $(this).next('span:first').find("select").html();
                        //var noOption = "<option value=0 >" + getLocale(DetailsBrowse, "Not required") + "</option>";
                        $(this).next('span:first').find("select").html(options);
                    } else {
                        $(this).next('span:first').find("select").find("option[value=0]").prop('selected', 'selected');
                    }
                    checkAvailibility(null);
                });
                $('.cssClassDropDownItem').MakeFancyItemDropDown();
                setTimeout(function () {
                    categoryDetails.LoadCostVariantCombination(itemSKU);
                    categoryDetails.variantCheckQuery();
                    //      selectFirstcombination(ItemID);
                }, 200);

            }
            else {
                categoryDetails.LoadCostVariantCombination(itemSKU);
            }
        },
        variantCheckQuery: function () {

            if (variantQuery != null && variantQuery != '') {
                var variantIds = variantQuery.split('@');
                var elem = null;
                $.each(variantIds, function (index, value) {
                    if ($('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').parents().attr("id") != undefined) {
                        $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').prop("selected", "selected");
                        var id = $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').parents().attr("id");
                        id = parseFloat(id.substring(id.lastIndexOf('_') + 1, id.length));
                        if (variantId.indexOf(id) != -1) {
                            variantId.splice(variantId.indexOf(id), 1);
                        }
                        elem = $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']');
                    }
                    if ($('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').attr("name") != undefined) {
                        $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').prop("checked", true);
                        var name = $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').attr("name");
                        name = parseFloat(name.substring(name.lastIndexOf('_') + 1, name.length));
                        if (variantId.indexOf(name) != -1) {
                            variantId.splice(variantId.indexOf(name), 1);
                        }
                        checkAvailibility($('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']'));
                        elem = $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']');
                    }
                });
                $.each(variantId, function (index, value) {

                    if ($("#controlCostVariant_" + value).parents().is('.sfListmenu')) {
                        //$("#controlCostVariant_" + value).prepend("<option value=0 >" + getLocale(DetailsBrowse, "Not required") + "</option>");
                        //$("#controlCostVariant_" + value).find("option[value=0]").prop('selected', 'selected');

                        elem = null;
                    }
                    else {
                        $('.cssClassRadio input[name="controlCostVariant_' + value + '"]').removeAttr('checked');
                        elem = null;
                    }
                });

                checkAvailibility(elem);
                if (categoryDetails.info.IsCombinationMatched == false)
                    selectFirstcombination(ItemId);
            }

        },

        GridView: function () {

            $("#divShowCategoryItemsList").html('');
            $("#divItemViewOptions").show();
            $("#divSearchPageNumber").show();
            var itemIds = [];
            var CostVariants = '';
            var tempScriptGridView = categoryDetails.GetTemplate('scriptResultGrid');
            $.each(arrItemsOptionToBind[0], function (index, value) {
                //debugger;
                if (!IsExistedCategory(itemIds, value.ItemID)) {


                    itemIds.push(value.ItemID);
                    var imagePath = itemImagePath + value.BaseImage;
                    if (value.BaseImage == "") {
                        imagePath = noImageCategoryDetailPath;
                    }
                    else {
                        imagePath = imagePath.replace('uploads', 'uploads/Medium');
                    }
                    var name = '';
                    if (value.Name.length > 50) {
                        name = value.Name.substring(0, 50);
                        var i = 0;
                        i = name.lastIndexOf(' ');
                        name = name.substring(0, i);
                        name = name + "...";
                    } else {
                        name = value.Name;
                    }

                    var items = [{
                        itemID: value.ItemID,
                        name: name,
                        titleName: value.Name,
                        SanchiCommerceRoot: aspxRedirectPath,
                        sku: value.SKU,
                        imagePath: aspxRootPath + imagePath,
                        loaderpath: templatePath + "/images/loader_100x12.gif",
                        alternateText: value.Name,
                        listPrice: (value.ListPrice),
                        price: (value.Price),
                        //isCostVariantHtml: CostVariants,//'"' + value.IsCostVariantItem + '"',
                        isCostVariant: '"' + value.IsCostVariantItem + '"',
                        shortDescription: Encoder.htmlDecode(value.ShortDescription),
                        ItemTypeID: value.ItemTypeID,
                        Quantity: value.Quantity
                    }];
                    $.tmpl(tempScriptGridView, items).appendTo("#divShowCategoryItemsList");
                    if (value.AttributeValues != null) {
                        if (value.AttributeValues != "") {
                            var attrHtml = '';
                            attrValues = [];
                            attrValues = value.AttributeValues.split(',');
                            attrHtml = "<div class='cssGridDyanamicAttr'>";
                            for (var i = 0; i < attrValues.length; i++) {
                                var attributes = [];
                                attributes = attrValues[i].split('#');
                                var attributeName = attributes[0];
                                var attributeValue = attributes[1];
                                var inputType = parseInt(attributes[2]);
                                var validationType = attributes[3];
                                attrHtml += "<div class=\"cssDynamicAttributes\">";
                                attrHtml += "<span>";
                                attrHtml += attributeName;
                                attrHtml += "</span> :";
                                if (inputType == 7) {
                                    attrHtml += "<span class=\"cssClassFormatCurrency\">";
                                }
                                else {
                                    attrHtml += "<span>";
                                }
                                attrHtml += attributeValue;
                                attrHtml += "</span></div>";
                            }
                            attrHtml += "</div>";
                            $('.cssGridDyanamicAttr').html(attrHtml);
                        }
                        else {
                            $('.cssGridDyanamicAttr').remove();
                        }
                    }
                    else {
                        $('.cssGridDyanamicAttr').remove();
                    }
                    BindCurrencySymbol();
                    if (value.IsCostVariantItem == true) {
                        var indx = value.CostVariants.split('#').length;
                        var data = value.CostVariants.split('#');
                        var myJsonString = $.extend({}, data);
                        categoryDetails.BindCostVariant(myJsonString, value.SKU, value.ItemID);
                    }
                    if (value.ListPrice == "") {
                        $(".cssRegularPrice_" + value.ItemID + "").remove();
                    }

                    if (allowAddToCart.toLowerCase() == 'true') {

                        if (allowOutStockPurchase.toLowerCase() == 'false') {
                            if (value.IsOutOfStock) {

                                $("#spanAvailability_" + value.ItemID + "").addClass('cssOutOfStock');
                                $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper span").html(getLocale(DetailsBrowse, 'Out Of Stock'));
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label").css("display", "inline-block");
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label span").css("color", "#ff3c00");
                                //$(".cssClassAddtoCard_" + value.ItemID).removeClass('cssClassAddtoCard');
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                                $(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                                $(".cssClassAddtoCard_" + value.ItemID + " a").removeAttr('onclick');
                            }
                            else { $("#spanAvailability_" + value.ItemID + "").addClass('cssInStock'); $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>'); }
                        }

                    }
                    else { $(".cssClassAddtoCard_" + value.ItemID).hide(); }
                    if (value.ItemTypeID == 5) {
                        $(".cssClassAddtoCard_" + value.ItemID + "").parents(".cssLatestItemInfo").find(".cssClassProductRealPrice").prepend(getLocale(AspxTemplateLocale, "Starting At"));
                    }
                }
            });
            //$("#divShowCategoryItemsList").wrapInner("<div class='cssCatProductGridWrapper'></div>");
            //code is added for product box bottom 0 by aniket
            var x = 0;
            var divtags = $('#divShowCategoryItemsList .cssClassProductsBox ');
            var htmlStr = "";
            var parentDiv = "";
            var wrapAllIdenfier = 1;

            $('#divShowCategoryItemsList .cssClassProductsBox ').each(function () {
                x++;

                if ((x % 3) == 0) {
                    // $(this).addClass('cssClassNoMargin');
                    $(this).addClass('cssClassNoMargin').addClass('i' + wrapAllIdenfier);
                    $('.i' + wrapAllIdenfier).wrapAll("<div class='cssCatProductGridWrapper'></div>");
                    wrapAllIdenfier++;
                }
                else $(this).addClass('i' + wrapAllIdenfier)
            });

            if (itemIds.length > $('.cssCatProductGridWrapper').length * 3) { $('.i' + wrapAllIdenfier).wrapAll("<div class='cssCatProductGridWrapper'></div>"); };
            //end

            if (itemIds.length == 0) {
                $("#divItemViewOptions").hide();
                $("#divSearchPageNumber").hide();
                $("#divShowCategoryItemsList").html("<span class=\"cssClassNotFound\">" + getLocale(DetailsBrowse, "No items found or matched!") + "</span>");
            }

            var $container = $(".cssCatProductGridWrapper");

            $container.imagesLoaded(function () {
                $container.masonry({
                    itemSelector: '.cssClassProductsBox',
                    EnableSorting: false
                });
            });
            currencyRate = 0;
        },

        ListView: function () {
            $("#divShowCategoryItemsList").html('');
            $("#divItemViewOptions").show();
            $("#divSearchPageNumber").show();
            var itemIds = [];
            var CostVariants = '';
            var tempScriptListView = categoryDetails.GetTemplate('scriptResultList');
            $.each(arrItemsOptionToBind[0], function (index, value) {
                if (!IsExistedCategory(itemIds, value.ItemID)) {

                    itemIds.push(value.ItemID);
                    var imagePath = itemImagePath + value.BaseImage;
                    if (value.BaseImage == "") {
                        imagePath = noImageCategoryDetailPath;
                    }
                    else {

                        imagePath = imagePath.replace('uploads', 'uploads/Medium');
                    }
                    var name = '';
                    if (value.Name.length > 50) {
                        name = value.Name.substring(0, 50);
                        var i = 0;
                        i = name.lastIndexOf(' ');
                        name = name.substring(0, i);
                        name = name + "...";
                    } else {
                        name = value.Name;
                    }

                    var items = [{
                        itemID: value.ItemID,
                        name: name,
                        titleName: value.Name,
                        SanchiCommerceRoot: aspxRedirectPath,
                        sku: value.SKU,
                        imagePath: aspxRootPath + imagePath,
                        alternateText: value.Name,
                        listPrice: (value.ListPrice),
                        price: (value.Price),
                        //isCostVariantHtml: CostVariants,//'"' + value.IsCostVariantItem + '"',
                        isCostVariant: '"' + value.IsCostVariantItem + '"',
                        shortDescription: Encoder.htmlDecode(value.ShortDescription),
                        ItemTypeID: value.ItemTypeID,
                        Quantity: value.Quantity
                    }];
                    $.tmpl(tempScriptListView, items).appendTo("#divShowCategoryItemsList");

                    if (value.AttributeValues != null) {
                        if (value.AttributeValues != "") {
                            var attrHtml = '';
                            attrValues = [];
                            attrValues = value.AttributeValues.split(',');
                            for (var i = 0; i < attrValues.length; i++) {
                                var attributes = [];
                                attributes = attrValues[i].split('#');
                                var attributeName = attributes[0];
                                var attributeValue = attributes[1];
                                var inputType = parseInt(attributes[2]);
                                var validationType = attributes[3];
                                attrHtml += "<div class=\"cssDynamicAttributes\">";
                                if (inputType == 7) {
                                    attrHtml += "<span class=\"cssClassFormatCurrency\">";
                                }
                                else {
                                    attrHtml += "<span>";
                                }
                                attrHtml += attributeValue;
                                attrHtml += "</span></div>";
                            }
                            $('.cssListDyanamicAttr').html(attrHtml);
                        }
                        else {
                            $('.cssListDyanamicAttr').remove();
                        }
                    }
                    else {
                        $('.cssListDyanamicAttr').remove();
                    }
                    BindCurrencySymbol();
                    if (value.IsCostVariantItem) {
                        var indx = value.CostVariants.split('#').length;
                        var data = value.CostVariants.split('#');
                        var myJsonString = $.extend({}, data);
                        categoryDetails.BindCostVariant(myJsonString, value.SKU, value.ItemID);
                    }
                    if (value.ListPrice == "") {
                        $(".cssRegularPrice_" + value.ItemID + "").remove();
                    }
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {
                            if (value.IsOutOfStock) {
                                $("#spanAvailability_" + value.ItemID + "").addClass('cssOutOfStock');
                                $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper span").html('Out Of Stock');
                                //$(".cssClassAddtoCard_" + value.ItemID).removeClass('cssClassAddtoCard');
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label").css("display", "inline-block");
                                $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label span").css("color", "#ff3c00");
                                $(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                                $(".cssClassAddtoCard_" + value.ItemID + " a").removeAttr('onclick');
                            }
                            else { $("#spanAvailability_" + value.ItemID + "").addClass('cssInStock'); $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>'); }
                        }
                    }
                    else { $(".cssClassAddtoCard_" + value.ItemID).hide(); }
                    if (value.ItemTypeID == 5) {
                        $(".cssClassAddtoCard_" + value.ItemID + "").parents(".cssLatestItemInfo").find(".cssClassProductRealPrice").prepend(getLocale(AspxTemplateLocale, "Starting At"));
                    }
                }
            });
            if (itemIds.length == 0) {
                $("#divItemViewOptions").hide();
                $("#divSearchPageNumber").hide();
                $("#divShowCategoryItemsList").html("<span class=\"cssClassNotFound\">" + getLocale(DetailsBrowse, "No items found or matched!") + "</span>");
            }
        },
        pageselectCallback: function (page_index, jq, execute) {
            //
            if (execute) {

                var max_elem = arrItemsOption.length;
                arrItemsOptionToBind[0].length = 0;

                for (var i = 0; i < max_elem; i++) {
                    arrItemsOptionToBind[0].push(arrItemsOption[i]);
                }
                $.each(arrItemsOptionToBind[0], function (index, item) {
                    Imagelist += item.BaseImage + ';';
                });

            }
            return false;
        },
        //Send the list of images to the ImageResizer
        ResizeImageDynamically: function (Imagelist, type) {
            ImageType = {
                "Large": "Large",
                "Medium": "Medium",
                "Small": "Small"
            };
            categoryDetails.config.method = "DynamicImageResizer";
            categoryDetails.config.url = aspxservicePath + "AspxImageResizerHandler.ashx/" + this.config.method;
            categoryDetails.config.data = JSON2.stringify({ imgCollection: Imagelist, type: ImageType.Medium, imageCatType: "Item", aspxCommonObj: aspxCommonObj });
            categoryDetails.config.ajaxCallMode = categoryDetails.ResizeImageSuccess;
            categoryDetails.ajaxCall(categoryDetails.config);

        },

        //Send the list of images to the ImageResizer
        ResizeCategoryImageDynamically: function (Imagelist, type) {
            categoryDetails.config.method = "DynamicImageResizer";
            categoryDetails.config.url = aspxservicePath + "AspxImageResizerHandler.ashx/" + this.config.method;
            categoryDetails.config.data = JSON2.stringify({ imgCollection: Imagelist, type: type, imageCatType: "Category", aspxCommonObj: aspxCommonObj });
            categoryDetails.config.ajaxCallMode = categoryDetails.ResizeImageSuccess;
            categoryDetails.ajaxCall(categoryDetails.config);

        },

        ResizeImageSuccess: function () {
        },

        BindShoppingFilterResult: function () {
          //  debugger;
            var viewAsOption = '';
            if (displaymode == "icon") {
                viewAsOption = $("#divViewAs").find('a.sfactive').attr("displayId");
                if (typeof viewAsOption == 'undefined') {
                    $("#divViewAs").find('a:eq(0)').addClass("sfactive");
                    viewAsOption = $("#divViewAs").find('a.sfactive').attr("displayId");
                }
            }
            else {
                viewAsOption = $("#ddlViewAs").val();
            }
            switch (viewAsOption) {
                case '1':
                    categoryDetails.GridView();
                    break;
                case '2':
                    categoryDetails.ListView();
                    break;
            }
            //var offset = 1;
            //var items_per_page = $('#ddlPageSize').val();
            //categoryDetails.GetDetail(offset, items_per_page, 0, $("#ddlSortBy").val());


        },

        BindSubCategoryForFilter: function (msg) {
            var elem = '';
            $(".filter").html('');
            var length = msg.d.length;
            if (length > 0) {
                elem = '<div id="divCat" value="b01" class="cssClasscategorgy">';
                elem += '<div class="divTitle"><b><label style="color:#006699">' + getLocale(DetailsBrowse, 'Categories') + '</label></b><img align="right" src="' + templatePath + '/images/arrow_down.png"/></div> <div id="scrollbar1" class="cssClassScroll"><div class="viewport"><div class="overview" id="catOverview"><div class="divContentb01"><ul id="cat"></ul></div></div></div></div></div>';
                $(".filter").append(elem);
                var value;
                for (var index = 0; index < length; index++) {
                    value = msg.d[index];
                    elem = '';
                    elem = '<li><label><input class="chkCategory" type="checkbox" name="' + value.CategoryName + '" ids="' + value.CategoryID + '" value="' + value.CategoryName + '"/> ' + value.CategoryName + '<span> (' + value.ItemCount + ')</span></label></li>';
                    $(".filter").find('div[value="b01"]').find('ul').append(elem);
                };
            }
        },

        BindBrandForCategory: function (msg) {
            var elem = '';
            var arrBrand = [];
            var length = msg.d.length;
            if (length > 0) {
                elem = '<div value="0" class="cssClasscategorgy">';
                elem += '<div class="divTitle"><b><label style="color:#006699">' + getLocale(DetailsBrowse, 'Brands') + '</label></b><img align="right" src="' + templatePath + '/images/arrow_down.png"/></div><div id="scrollbar2" class="cssClassScroll"><div class="viewport"><div class="overview"><div class="divContent0"><ul></ul></div></div></div></div></div>';
                $(".filter").append(elem);
                var value;
                for (var index = 0; index < length; index++) {
                    value = msg.d[index];
                    if (indexOfArray(arrBrand, value.BrandID) == -1) {
                        elem = '';
                        elem = '<li><label><input class="chkFilter chkBrand" type="checkbox" name="' + value.BrandName + '" ids="' + value.BrandID + '" value="0"/> ' + value.BrandName + '<span id="count"> (' + value.ItemCount + ')</span></label></li>';
                        $(".filter").find('div[value="0"]').find('ul').append(elem);
                        arrBrand.push(value.BrandID);
                    }
                };
            }
        },

        BindShoppingFilter: function (msg) {
           // debugger;
            var attrID = [];
            var attrValue = [];
            var attrName = '';
            var elem = '';
            var length = msg.d.length;
            if (length > 0) {
                $(".divRange").show();
                var value;
                for (var index = 0; index < length; index++) {
                    value = msg.d[index];
                    isCategoryHasItems = 1;
                    if (value.AttributeID != 8 && value.AttributeID > 48) {
                        if (indexOfArray(attrID, value.AttributeID) == -1) {
                            elem = '<div value=' + value.AttributeID + ' class="cssClasscategorgy"><div class="divTitle"><b><label style="color:#006699">' + value.AttributeName + '</label></b><img align="right" src="' + templatePath + '/images/arrow_down.png"/></div> <div id="scrollbar3" class="cssClassScroll"><div class="viewport"><div class="overview"><div class=' + "divContent" + value.AttributeID + '><ul></ul></div></div></div></div></div>';
                            attrValue = [];
                            attrID.push(value.AttributeID);
                            $(".filter").append(elem);
                            elem = '';
                            elem = '<li><label><input class= "chkFilter" type="checkbox" name="' + value.AttributeValue + '" inputTypeId="' + value.InputTypeID + '"  value="' + value.AttributeID + '"/> ' + value.AttributeValue + '<span id="count"> (' + value.ItemCount + ')</span></label></li>';
                            $(".filter").find('div[value=' + value.AttributeID + ']').find('ul').append(elem);
                            attrValue.push(value.AttributeValue);
                        } else {
                            if (indexOfArray(attrValue, value.AttributeValue) == -1) {
                                itemCount = 1;
                                elem = '';
                                elem = '<li><label><input class="chkFilter" type="checkbox" name="' + value.AttributeValue + '" inputTypeId="' + value.InputTypeID + '"  value="' + value.AttributeID + '"/> ' + value.AttributeValue + '<span id="count"> (' + value.ItemCount + ')</span></label></li>';
                                $(".filter").find('div[value=' + value.AttributeID + ']').find('ul').append(elem);
                                attrValue.push(value.AttributeValue);
                            }
                        }
                    } else if (value.AttributeID == 8) {
                        arrPrice.push(value);
                        if (parseFloat(value.AttributeValue) > maxPrice) {
                            maxPrice = parseFloat(value.AttributeValue);
                        }
                    }
                };
                var interval = parseFloat((maxPrice - minPrice) / 4);
                elem = '<div value="8" class="cssClassbrowseprice">';
                elem += '<div class="divTitle"><b><label style="color:#006699">' + getLocale(DetailsBrowse, 'Price') + '</label></b><img align="right" src="' + templatePath + '/images/arrow_up.png"/></div><div class="divContent8"><ul>';

                if (arrPrice.length > 1) {
                    elem += '<li><a id="f1" href="#" ids=""  minprice=' + GetPriceData(minPrice, 0, interval) + ' maxprice=' + GetPriceData(minPrice, 1, interval) + '>' + '<span class=\"cssClassFormatCurrency\">' + parseFloat(minPrice).toFixed(2) + '</span>' + ' - ' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice, 1, interval) + '</span>' + '</a></li>';
                    elem += '<li><a id="f2" href="#" ids="" minprice=' + GetPriceData(minPrice + 0.01, 1, interval) + ' maxprice=' + GetPriceData(minPrice, 2, interval) + '>' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice + 0.01, 1, interval) + '</span>' + ' - ' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice, 2, interval) + '</span>' + '</a></li>';
                    elem += '<li><a id="f3" href="#" ids="" minprice=' + GetPriceData(minPrice + 0.01, 2, interval) + ' maxprice=' + GetPriceData(minPrice, 3, interval) + '>' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice + 0.01, 2, interval) + '</span>' + ' - ' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice, 3, interval) + '</span>' + '</a></li>';
                    elem += '<li><a id="f4" href="#" ids="" minprice=' + GetPriceData(minPrice + 0.01, 3, interval) + ' maxprice=' + maxPrice + '>' + '<span class=\"cssClassFormatCurrency\">' + GetPriceDataFloat(minPrice + 0.01, 3, interval) + '</span>' + ' - ' + '<span class=\"cssClassFormatCurrency\">' + parseFloat(maxPrice).toFixed(2) + '</span>' + '</a></li>';
                }
                if (arrPrice.length == 1) {
                    elem += '<li><a id="f1" href="#" ids=","  minprice=' + GetPriceData(minPrice, 0, interval) + ' maxprice=' + GetPriceData(minPrice, 1, interval) + '>' + '<span class=\"cssClassFormatCurrency\">' + parseFloat(minPrice).toFixed(2) + '</span>' + '</a></li>';
                    minPrice = 0;
                }
                elem += '</ul></div>';
                elem += '<div class="divRange"><div id="slider-range"></div><p><b style="color: #006699">' + getLocale(DetailsBrowse, "Range: ") + '<span id="amount"></span></b></p></div></div>';
                $(".filter").append(elem);
                BindCurrencySymbol();
                minPrice = parseFloat(minPrice);
                maxPrice = parseFloat(maxPrice);
                $("#slider-range").slider({
                    range: true,
                    min: minPrice,
                    max: maxPrice,
                    step: 0.001,
                    values: [minPrice, maxPrice],
                    slide: function (event, ui) {
                        $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[0]).toFixed(2) + "</span>" + " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[1]).toFixed(2) + "</span>");
                        BindCurrencySymbol();
                    },
                    change: function (event, ui) {
                        $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[0]).toFixed(2) + "</span>" + " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat(ui.values[1]).toFixed(2) + "</span>");
                        BindCurrencySymbol();
                        if (event.originalEvent == undefined && count == 0) {
                            categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
                            count = count + 1;
                        }
                        else if (event.originalEvent != undefined) {
                            categoryDetails.GetDetail(1, $('#ddlPageSize').val(), 0, $("#ddlSortBy").val());
                        }

                    }
                });
                $("#amount").html("<span class=\"cssClassFormatCurrency\">" + parseFloat($("#slider-range").slider("values", 0)).toFixed(2) + "</span>" +
                           " - " + "<span class=\"cssClassFormatCurrency\">" + parseFloat($("#slider-range").slider("values", 1)).toFixed(2) + "</span>");

                BindCurrencySymbol();
            }
            else {
                if ($(".filter").length == 0) {
                    $("#divShopFilter").hide();
                }
                else {
                    $(".divRange").hide();
                }
            }
        },

        BindItemDetail: function (msg) {

            arrItemsOptionToBind[0].length = 0;
            rowTotal = 0;
            arrItemsOption.length = 0;
            $.each(msg.d, function (index, value) {
                arrItemsOption.push(value);
                rowTotal = value.RowTotal;
            });
            limit = $('#ddlPageSize').val();
            var offset = 1;
            var items_per_page = $('#ddlPageSize').val();
            //
            $("#Pagination").pagination(rowTotal, {
                callback: categoryDetails.pageselectCallback,
                items_per_page: items_per_page,
                current_page: currentpage,
                callfunction: true,
                function_name: { name: categoryDetails.GetDetail, limit: $('#ddlPageSize').val(), sortBy: $("#ddlSortBy").val() },
                prev_text: "Prev",
                next_text: "Next",
                prev_show_always: false,
                next_show_always: false
            });

        },


    };

    categoryDetails.init();
});
//added by aniket
function selectFirstcombination(ItemId) {

    if (arrCombination.length > 0) {

        var Combination = [];
        $.each(arrCombination, function (index, item) {

            if (ItemId == item.ItemID) {

                Combination.push(item);
            }

        });
        var cvcombinationList = getObjects(arrCombination, 'CombinationType');
        var cvValuecombinationList = getObjects(arrCombination, 'CombinationValues');
        var x = cvcombinationList[0].split('@');
        var y = cvValuecombinationList[0].split('@');
        $("#Notify").hide();
        $("#divCostVariant_" + ItemId + " select").each(function (i) {

            if (parseFloat($(this).parent("span:eq(0)").prop('id').split('_')[0].replace('subDiv', '')) == x[i]) {
                if ($(this).find("option[value=" + y[i] + "]").length > 0) {
                    $(this).find("option[value=" + y[i] + "]").prop('selected', 'selected');
                }
                else {

                    var options = $(this).html();
                    //var noOption = "<option value='0'>" + getLocale(DetailsBrowse, "Not required") + "</option>";
                    $(this).html(options);
                    //$(this).find('option[value=0]').prop('selected', 'selected');
                }
            }
            else {
                var val = parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', ''));
                var xIndex = 0;
                for (var indx = 0; indx < x.length; indx++) {
                    if (x[indx] == val) {
                        xIndex = indx;
                        break;
                    }
                }
                if ($(this).find("option[value=" + y[xIndex] + "]").length > 0) {
                    $(this).find("option[value=" + y[xIndex] + "]").prop('selected', 'selected');

                } else {

                    var options = $(this).html();
                    //var noOption = "<option value='0'>" + getLocale(DetailsBrowse, "Not required") + "</option>";
                    $(this).html(options);
                    //$(this).find('option[value=0]').prop('selected', 'selected');
                }
            }
        });

        $("#divCostVariant_" + ItemId + " input[type=radio]").each(function (i) {
            if (parseFloat($(this).parents("span:eq(0)").prop('id').replace('subDiv', '')) == x[i]) {
                if ($(this).val() == y[i]) {
                    $(this).prop('checked', 'checked').addClass("cssRadioChecked");
                }
                else {
                    $(this).removeAttr('checked');
                }
            } else {
                var val = parseFloat($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
                var xIndex = 0;
                for (var indx = 0; indx < x.length; indx++) {
                    if (x[indx] == val) {
                        xIndex = indx;
                        break;
                    }
                }
                if ($(this).val() == y[xIndex]) {
                    $(this).prop('checked', 'checked').addClass("cssRadioChecked");

                } else {
                    $(this).removeAttr('checked');
                }
            }
        });

        $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function (i) {
            if (parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', '')) == x[i]) {
                if ($(this).val() == y[i]) {
                    $(this).prop('checked', 'checked');

                } else {
                    $(this).removeAttr('checked');
                }
            } else {
                var val = parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', ''));
                var xIndex = 0;
                for (var indx = 0; indx < x.length; indx++) {
                    if (x[indx] == val) {
                        xIndex = indx;
                        break;
                    }
                }

                if ($(this).val() == y[xIndex]) {
                    $(this).prop('checked', 'checked');

                } else {
                    $(this).removeAttr('checked');
                }
            }
        });


        CheckVariantCombination(cvcombinationList[0], cvValuecombinationList[0], x[0], y[0], ItemId);
        //$("#spanAvailability_" + ItemId + "").html(categoryDetails.info.IsCombinationMatched == true ? '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' : '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>');
        if (categoryDetails.info.IsCombinationMatched == true) {
            $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
            $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
        }
        else {
            $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
            $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
            //$("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
            //$("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
        }
        if (categoryDetails.info.IsCombinationMatched) {

            $("#hdnQuantity_" + ItemId + "").val('').val(categoryDetails.info.Quantity);
            $("#txtQty_" + ItemId + "").removeAttr('disabled').prop("enabled", "enabled");
            if (categoryDetails.info.Quantity == 0 || categoryDetails.info.Quantity < 0) {
                if (allowAddToCart.toLowerCase() == 'true') {
                    if (allowOutStockPurchase.toLowerCase() == 'false') {

                        $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                        $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                        $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                        $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                        //if (userName != "anonymoususer") {
                        //    $("#Notify").show();
                        //    $("#Notify #txtNotifiy").hide();
                        //} else {
                        //    $("#Notify").show();
                        //    $("#txtNotifiy").show();
                        //}
                    }
                }
                else {
                    $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                }

            } else {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
                $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>');
                $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
                $("#spanAvailability_" + ItemId + "").addClass('cssInStock');
                $("#Notify").hide();
            }
            var values = getSelectecdVariantValues().Values;

            var itemsCartInfo = categoryDetails.CheckItemQuantityInCart(ItemId, values.join('@') + '@');
            var quantityinCart = itemsCartInfo.ItemQuantityInCart;
            if (categoryDetails.info.Quantity <= quantityinCart) {
                if (allowAddToCart.toLowerCase() == 'true') {
                    if (allowOutStockPurchase.toLowerCase() == 'false') {

                        $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');

                        $("#txtQty_" + ItemId + "").removeAttr('enabled').prop("disabled", "disabled");
                        $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                        $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                        $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                        //if (userName != "anonymoususer") {
                        //    $("#Notify").show();
                        //    $("#Notify #txtNotifiy").hide();
                        //} else {
                        //    $("#Notify").show()
                        //    $("#txtNotifiy").show();
                        //}
                    }
                }
                else {
                    $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                }
            }
            var price = 0;
            if (categoryDetails.info.IsPricePercentage) {
                price = eval($("#hdnPrice_" + ItemId + "").val()) * eval(categoryDetails.info.PriceModifier) / 100;
            } else {
                price = eval(categoryDetails.info.PriceModifier);
            }

            $("#spanPrice_" + ItemId + "").html(parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
            $("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
            //$("#spanPrice_" + ItemId + "").html(parseFloat((eval(price))).toFixed(2));
            //$("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval(price))).toFixed(2));
            var taxPriceVariant = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
            var taxrate = (parseFloat(($("#hdnTaxRateValue").val()) * 100) / (eval($("#hdnPrice_" + ItemId + "").val())).toFixed(2));
            if ($("#hdnListPrice").val() != '') {
                $(".cssClassYouSave").show();
                var variantAddedPrice = eval($("#hdnPrice").val()) + eval(price);
                var variantAddedSavingPercent = (($("#hdnListPrice").val() - variantAddedPrice) / $("#hdnListPrice").val()) * 100;
                savingPercent2 = variantAddedSavingPercent.toFixed(2);
                $("#spanSaving").html('<b>' + variantAddedSavingPercent.toFixed(2) + '%</b>');
            }
        }
    }
    var cookieCurrency = $("#ddlCurrency").val();
    $("#spanPrice_" + ItemId + "").removeAttr('data-currency-' + cookieCurrency + '');
    $("#spanPrice_" + ItemId + "").removeAttr('data-currency');
    Currency.currentCurrency = BaseCurrency;
    Currency.convertAll(Currency.currentCurrency, cookieCurrency);
}
//end
//added by aniket
function checkAvailibility(elem) {
    if (elem.length != 0) {
        var cvids = [];
        var values = [];
        var currentValue = elem == null ? 1 : $(elem).val();
        var currentCostVariant = elem == null ? 1 : $(elem).parents('span:eq(0)').prop('id').split('_')[0].replace('subDiv', '');
        ItemId = $(elem).parents('span:eq(0)').prop('id').split('_')[1];
        $("#Notify").hide();
        $("#divCostVariant_" + ItemId + " select option:selected").each(function () {
            if (this.value != 0) {
                values.push(this.value);
                cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
            }
        });

        $("#divCostVariant_" + ItemId + " input[type=radio]:checked").each(function () {
            if ($(this).is(":checked"))
            { $(this).addClass("cssRadioChecked") }
            else { $(this).removeClass("cssRadioChecked"); }
            values.push(this.value);
            cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
        });

        $("#divCostVariant_" + ItemId + " input[type=radio]").each(function () {
            if ($(this).is(":checked"))
            { $(this).addClass("cssRadioChecked") }
            else { $(this).removeClass("cssRadioChecked"); }
        });

        $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function () {
            values.push(this.value);
            cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
        });

        var infos = CheckVariantCombination(cvids.join('@'), values.join('@'), currentCostVariant, currentValue, ItemId);


        //$("#spanAvailability_" + ItemId + "").html(categoryDetails.info.IsCombinationMatched == true ? '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' : '<b>' + getLocale(DetailsBrowse, 'Not available') + '</b>');
        if (categoryDetails.info.IsCombinationMatched == true) {
            $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
            $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
        }
        else {
            $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
            $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');

            //$("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
            //$("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
        }

        if (categoryDetails.info.IsCombinationMatched) {
            $("#hdnQuantity_" + ItemId + "").val('').val(categoryDetails.info.Quantity);
            $("#txtQty_" + ItemId + "").removeAttr('disabled').prop("enabled", "enabled");
            if (categoryDetails.info.Quantity == 0 || categoryDetails.info.Quantity < 0) {
                if (allowAddToCart.toLowerCase() == 'true') {
                    if (allowOutStockPurchase.toLowerCase() == 'false') {
                        $("#spanAvailability_" + ItemId + "").html("<b>" + getLocale(DetailsBrowse, "Out Of Stock") + "</b>");
                        $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                        //if (userName != "anonymoususer") {
                        //    $("#Notify").show();
                        //    $("#Notify #txtNotifiy").hide();
                        //}
                        //else {
                        //    $("#Notify").show();
                        //    $("#txtNotifiy").show();
                        //}
                    }
                }
                else {
                    $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                }

            } else {
                $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' + '</b>');
                $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
                $("#spanAvailability_" + ItemId + "").addClass('cssInStock');
                $("#Notify").hide();
            }
            var itemsCartInfo = categoryDetails.CheckItemQuantityInCart(ItemId, values.join('@') + '@');
            var quantityinCart = itemsCartInfo.ItemQuantityInCart;
            if (categoryDetails.info.Quantity <= quantityinCart) {
                if (allowAddToCart.toLowerCase() == 'true') {
                    if (allowOutStockPurchase.toLowerCase() == 'false') {

                        $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');

                        $("#txtQty_" + ItemId + "").removeAttr('enabled').prop("disabled", "disabled");
                        $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                        $("#spanAvailability_" + ItemId + "").html("<b>" + getLocale(DetailsBrowse, "Out Of Stock") + "</b>");
                        $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                        //if (userName != "anonymoususer") {
                        //    $("#Notify").show();
                        //    $("#Notify #txtNotifiy").hide();
                        //}
                        //else {
                        //    $("#Notify").show();
                        //    $("#txtNotifiy").show();
                        //}
                    }
                }
                else {
                    $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                }
            }
            var price = 0;
            if (categoryDetails.info.IsPricePercentage) {
                price = eval($("#hdnPrice_" + ItemId + "").val()) * eval(categoryDetails.info.PriceModifier) / 100;
            } else {
                price = eval(categoryDetails.info.PriceModifier);
            }

            $("#spanPrice_" + ItemId + "").html(parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
            $("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
            //$("#spanPrice_" + ItemId + "").html(parseFloat((eval(price))).toFixed(2));
            //$("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval(price))).toFixed(2));

            var taxPriceVariant = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
            var taxrate = (eval($("#hdnTaxRateValue").val()) * 100) / (eval($("#hdnPrice_" + ItemId + "").val()));
            $("#spanTax").html(parseFloat(((taxPriceVariant * taxrate) / 100)).toFixed(2));
            if ($("#hdnListPrice_" + ItemId + "").val() != '') {
                $(".cssClassYouSave").show();
                var variantAddedPrice = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
                var variantAddedSavingPercent = (($("#hdnListPrice_" + ItemId + "").val() - variantAddedPrice) / $("#hdnListPrice_" + ItemId + "").val()) * 100;
                savingPercent2 = variantAddedSavingPercent.toFixed(2);
                $("#spanSaving").html('<b>' + variantAddedSavingPercent.toFixed(2) + '%</b>');
            }
            //categoryDetails.ResetGallery(categoryDetails.info.CombinationID);

        } else {
            $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
            $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
            $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
            $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');

        }
        var cookieCurrency = $("#ddlCurrency").val();
        $("#spanPrice_" + ItemId + "").removeAttr('data-currency-' + cookieCurrency + '');
        $("#spanPrice_" + ItemId + "").removeAttr('data-currency');
        Currency.currentCurrency = BaseCurrency;
        Currency.convertAll(Currency.currentCurrency, cookieCurrency);
    }
    else { categoryDetails.info.IsCombinationMatched = true; }
}
//end
//added by aniket
function getSelectecdVariantValues() {

    var cvids = [];
    var values = [];

    $("#divCostVariant_" + ItemId + " select option:selected").each(function () {
        if (this.value != 0) {
            values.push(this.value);
            cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
        }
    });

    $("#divCostVariant_" + ItemId + " input[type=radio]:checked").each(function () {
        if ($(this).is(":checked"))
        { $(this).addClass("cssRadioChecked") }
        else { $(this).removeClass("cssRadioChecked"); }
        values.push(this.value);
        cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
    });

    $("#divCostVariant_" + ItemId + " input[type=radio]").each(function () {
        if ($(this).is(":checked"))
        { $(this).addClass("cssRadioChecked") }
        else { $(this).removeClass("cssRadioChecked"); }
    });

    $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function () {
        values.push(this.value);
        cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
    });
    return { Values: values, CVIds: cvids };
}
//end
function getModifiersByObjects(obj, combinationId) {
    var modifiers = { Price: '', IsPricePercentage: false, Weight: '', IsWeightPercentage: false, Quantity: 0 };

    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            if (obj[i]["CombinationID"] == combinationId) {
                modifiers.Price = obj[i]["CombinationPriceModifier"];
                modifiers.IsPricePercentage = obj[i]["CombinationPriceModifierType"];
                modifiers.Weight = obj[i]["CombinationWeightModifier"];
                modifiers.IsWeightPercentage = obj[i]["CombinationWeightModifierType"];
                modifiers.Quantity = obj[i]["CombinationQuantity"];
            }
        }

    }
    return modifiers;
}

function GetPriceData(price, count, interval) {
    return (parseFloat(price + (count * interval)));

}

function GetPriceDataFloat(price, count, interval) {
    return (parseFloat(price + (count * interval)).toFixed(2));

}

