﻿var baseurl = aspxservicePath + "AspxCoreHandler.ashx/";
$('#BtnOrderID').on('click', function () {
    debugger;
    var aspxCommonObj = {
        StoreID: SanchiCommerce.utils.GetStoreID(),
        PortalID: SanchiCommerce.utils.GetPortalID(),
        UserName: SanchiCommerce.utils.GetUserName(),
        CultureName: SanchiCommerce.utils.GetCultureName()
    };
    var orderId = $('#OrderSearch').val();    
    $.ajax({
        type: "POST",
        async: false,
        url: baseurl + "GetAllOrderDetailsForView",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON2.stringify({ orderId: orderId, aspxCommonObj: aspxCommonObj }),
        success: function (data) {
            $("#OrderStatusResult").empty();
            var html = '';
            if (data.d.length > 0) {
                html += '<p><b>Order ID:</b> ' + data.d[0].OrderID + '</p>';
                html += '<p><b>Ordered Date:</b> ' + data.d[0].OrderedDate + '</p>';
                html += '<p><b>Billing To:</b> ' + data.d[0].BillingAddress.split(',')[0] + '</p>';
                html += '<p><b>Invoice Number:</b> ' + data.d[0].InVoiceNumber + '</p>';
                html += '<p><b>Amount:</b> Rs.' + data.d[0].GrandTotal + '</p>';
                html += '<p><b>Order Status:</b> ' + data.d[0].OrderStatus + '</p>';
                $('#OrderStatusResult').append(html);

                if (data.d[0].OrderStatus == "Processed") {
                    document.getElementById("trackimg").src = "/modules/sanchicommerce/AspxOrderTracking/images/cat_46_198761.jpg";
                }
                else if (data.d[0].OrderStatus == "Complete") {
                    document.getElementById("trackimg").src = "/modules/sanchicommerce/AspxOrderTracking/images/banner5.png";
                }
                else if (data.d[0].OrderStatus == "Approval") {
                    document.getElementById("trackimg").src = "/modules/sanchicommerce/AspxOrderTracking/images/banner_6.png";
                }
                else if (data.d[0].OrderStatus == "Shipping") {
                    document.getElementById("trackimg").src = "/modules/sanchicommerce/AspxOrderTracking/images/banner_6.png";
                }
            }
            else {
                html += '<p><b> Wrong Order ID </b></p>';
                $('#OrderStatusResult').append(html);
            }
            
        }
      
     });
});
$('#ClearBtn').on('click', function () {
    //debugger;
    $('#OrderSearch').val('');
    $("#OrderStatusResult").empty();
});