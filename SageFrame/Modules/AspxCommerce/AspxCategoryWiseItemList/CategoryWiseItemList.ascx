<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoryWiseItemList.ascx.cs"
    Inherits="Modules_AspxCommerce_AspxCategoryWiseItemsList_CategoryWiseItemList" %>

<div id="divcategoryItemsList" class="classItemList">
    <div id="tblcategoryItems">
        <asp:Literal ID="ltrCategoryWiseItem" EnableViewState="False" runat="server" meta:resourcekey="ltrCategoryWiseItemResource1"></asp:Literal>
    </div>
    <div class="cssClassclear">
    </div>
    <div class="cssClassPageNumber" id="divSearchPageNumber" >
        <div class="cssClassPageNumberLeftBg">
            <div class="cssClassPageNumberRightBg">
                <div class="cssClassPageNumberMidBg">
                    <div id="Pagination">
                    </div>
                    <div class="cssClassViewPerPage">
                        <span class="sfLocale">View Per Page</span>
                        <span class="cssClassDrop">
                            <select id="ddlPageSize" class="cssClassDropDown">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    (function ($) {
        $.CategoryWiseItemView = function (p) {
            p = $.extend
            ({
                countryName: '',
                defaultImagePath: '',
                allowAddToCart: '',
                allowOutStockPurchase: '',
                catWiseItemModulePath: '',
                noOfItemsInCategory: 0,
                rowTotal: 0,
                noOfItemsDisplayInARow: 0
            }, p);

            var ip = AspxCommerce.utils.GetClientIP();
            var currentPage = 0;
            var costVariantValueIDs = "";
            var count = 0;
            var nomargin = 0;
            var aspxCommonObj = function () {
                var aspxCommonInfo = {
                    StoreID: AspxCommerce.utils.GetStoreID(),
                    PortalID: AspxCommerce.utils.GetPortalID(),
                    UserName: AspxCommerce.utils.GetUserName(),
                    CultureName: AspxCommerce.utils.GetCultureName(),
                    SessionCode: AspxCommerce.utils.GetSessionCode(),
                    CustomerID: AspxCommerce.utils.GetCustomerID()
                };
                return aspxCommonInfo;
            };
            IsExistedCategory = function (arr, cat) {
                var isExist = false;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] == cat) {
                        isExist = true;
                        break;
                    }
                }
                return isExist;
            };
            var categoryWiseItemList = {
                config: {
                    isPostBack: false,
                    async: true,
                    cache: true,
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    data: '{}',
                    dataType: 'json',
                    baseURL: aspxservicePath,
                    baseURL1: p.catWiseItemModulePath,
                    method: "",
                    url: "",
                    ajaxCallMode: "", ///0 for get categories and bind, 1 for notification,2 for versions bind
                    itemid: 0
                },

                vars: {
                    countCompareItems: 0
                },
                ajaxCall: function (config) {
                    $.ajax({
                        type: categoryWiseItemList.config.type,
                        contentType: categoryWiseItemList.config.contentType,
                        cache: categoryWiseItemList.config.cache,
                        async: categoryWiseItemList.config.async,
                        url: categoryWiseItemList.config.url,
                        data: categoryWiseItemList.config.data,
                        dataType: categoryWiseItemList.config.dataType,
                        success: categoryWiseItemList.config.ajaxCallMode,
                        error: categoryWiseItemList.config.ajaxFailure
                    });
                },
                GetCategoryWiseItemList: function (offset, limit, currentpage) {
                    debugger;
                    var aspxCommonInfo = aspxCommonObj();
                    delete aspxCommonInfo.UserName;
                    $.ajax({
                        type: "POST",
                        url: p.catWiseItemModulePath + "Services/CategoryWiseItemListHandler.ashx/GetCategoryWiseItemSettings",
                        data: JSON2.stringify({ aspxCommonObj: aspxCommonInfo }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            $.each(msg.d, function (index, value) {
                                p.noOfItemsInCategory = value.NumberOfItemsInCategory;
                            });
                            categoryWiseItemList.CategoryItemsList(offset, limit, currentpage);
                        }
                    });
                },
                CategoryItemsList: function (offset, limit, currentpage) {
                    currentPage = currentpage;
                    var aspxCommonInfo = aspxCommonObj();
                    delete aspxCommonInfo.SessionCode;
                    delete aspxCommonInfo.CustomerID;
                    categoryWiseItemList.config.method = "Services/CategoryWiseItemListHandler.ashx/GetCategoryWiseItemList";
                    categoryWiseItemList.config.url = p.catWiseItemModulePath + categoryWiseItemList.config.method;
                    categoryWiseItemList.config.data = JSON2.stringify({ offset: offset, limit: limit, noOfItemsInCategory: p.noOfItemsInCategory, aspxCommonObj: aspxCommonInfo });
                    categoryWiseItemList.config.ajaxCallMode = categoryWiseItemList.BindCategoryItems;
                    categoryWiseItemList.ajaxCall(categoryWiseItemList.config);
                },

                BindCategoryItems: function (msg) {
                    $("#tblcategoryItems").html('');
                    var catIDs = [];
                    var html = '';
                    $.each(msg.d, function (index, value) {
                        if (IsExistedCategory(catIDs, value.CategoryID)) {
                            count = count + 1;
                            if ((count + 1) % eval(p.noOfItemsDisplayInARow) == 0) {
                                nomargin = 1;
                            }
                            else {
                                nomargin = 0;
                            }
                            categoryWiseItemList.BindRecentItems(value);
                        } else {
                            catIDs.push(value.CategoryID);
                            count = 0;
                            nomargin = 0;
                            html = '<div class="cssCategoryBlock">';
                            html += '<label class="classCategoryName" id=classCategoryName_' + value.CategoryID + '><h2 class="cssClassMiddleHeader"><span>' + value.CategoryName + '</span></h2></label>';
                            html += '<div id="divViewMore_' + value.CategoryID + '" class="cssViewMore"></div>';
                            html += "<div class=\"cssClassClear\"></div>";
                            html += '<div id=div_' + value.CategoryID + ' class="categorywrap clearfix"></div>';

                            $("#tblcategoryItems").append(html);
                            if (value.ItemRowNum <= p.noOfItemsInCategory) {
                                categoryWiseItemList.BindRecentItems(value);
                            }
                        }
                    });
                },
                BindCurrencySymbol : function () {
                    var cookieCurrency = Currency.cookie.read();
                    Currency.currentCurrency = BaseCurrency;
                    Currency.format = 'money_format';
                    Currency.convertAll(Currency.currentCurrency, cookieCurrency);
                },

                fixedEncodeURIComponent: function (str) {
                    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/-/g, '_').replace(/\*/g, '%2A').replace(/%26/g, 'ampersand').replace(/%20/g, '-');
                },
                BindRecentItems: function (item) {
                    var RecentItemContents = '';
                    var imagePath = itemImagePath + item.ImagePath;
                    if (item.ImagePath == "") {
                        imagePath = p.defaultImagePath;
                    }
                    if (item.AlternateText == "") {
                        item.AlternateText = item.Name;
                    }
                    p.rowTotal = item.RowTotal;

                    var hrefItem = aspxRedirectPath + "item/" + categoryWiseItemList.fixedEncodeURIComponent(item.SKU) + pageExtension;
                    var name = '';
                    if (item.Name.length > 50) {
                        name = item.Name.substring(0, 50);
                        var i = 0;
                        i = name.lastIndexOf(' ');
                        name = name.substring(0, i);
                        name = name + "...";
                    } else {
                        name = item.Name;
                    }
                    RecentItemContents = '<div class="classItemsList_' + item.CategoryID + '">';
                    if (nomargin == 1) {
                        RecentItemContents += '<div class="cssLatestItemInfo cssClassProductsBox cssClassProductsBoxNoMargin">';
                    }
                    else {
                        RecentItemContents += '<div class="cssLatestItemInfo cssClassProductsBox">';
                    }
                    RecentItemContents += '<div id="productImageWrapID_' + item.ItemID + '" class="cssClassProductsBoxInfo" costvariantItem=' + item.IsCostVariantItem + '  itemid="' + item.ItemID + '">';
                    RecentItemContents += '<div id="divitemImage" class="cssClassProductPicture"><a href="' + hrefItem + '"><img alt="' + item.AlternateText + '"  title="' + item.AlternateText + '" src="' + aspxRootPath + imagePath.replace('uploads', 'uploads/Medium') + '"></a></div>';
                    RecentItemContents +=   '<div class="productName"><a href="' + aspxRedirectPath + 'item/' + item.SKU + pageExtension + '" title="' + item.Name + '"><h2>' + name + '</h2></a></div>';
                    if (!item.HidePrice) {
                        if (item.ListPrice == null) {
                            RecentItemContents += "<div class=\"cssClassProductPriceBox\"><div class=\"cssClassProductPrice\"><p class=\"cssClassProductRealPrice \" ><span class=\"cssClassFormatCurrency\" value=" + (item.Price).toFixed(2) + ">" + parseFloat(item.Price).toFixed(2) + "</span></p></div></div>";
                        } else {
                        
                            RecentItemContents += "<div class=\"cssClassProductPriceBox\"><div class=\"cssClassProductPrice\"><p class=\"cssClassProductOffPrice\"><span class=\"cssClassFormatCurrency\" value=" + (item.ListPrice).toFixed(2) + ">" + parseFloat(item.ListPrice).toFixed(2) + "</span></p><p class=\"cssClassProductRealPrice \" ><span class=\"cssClassFormatCurrency\" value=" + (item.Price).toFixed(2) + ">" + parseFloat(item.Price).toFixed(2) + "</span></p></div></div>";
                        }
                    } else {
                        RecentItemContents += "<div class=\"cssClassProductPriceBox\"></div>";
                    }
                    RecentItemContents += '<div class="cssClassProductDetail"><p><a href="' + aspxRedirectPath + 'item/' + item.SKU + pageExtension + '">' + getLocale(AspxCategoryWiseItem, "Details") + '</a></p></div>';

                    RecentItemContents += "<div class=\"sfButtonwrapper\">";
                   // RecentItemContents += "<div class=\"cssClassWishListButton\"><input type=\"hidden\" name=\"itemwish\"  value='" + item.ItemID + ',' + JSON2.stringify(item.SKU) + ",this' /></div>";
                    RecentItemContents += "<div class=\"cssClassCompareButton\"><input type=\"hidden\" name=\"itemcompare\"  value='" + item.ItemID + ',' + JSON2.stringify(item.SKU) + ",this' /></div>";
                    RecentItemContents += "</div>";
                    RecentItemContents += "<div class=\"cssClassclear\"></div></div>";
                    var itemSKU = JSON2.stringify(item.SKU);
                    var itemName = JSON2.stringify(item.Name);
                    var itemPriceValue = item.Price;
                    var dataContent = "";
                    dataContent += "data-class=\"addtoCart\" data-type=\"button\" data-addtocart=\"";
                    dataContent += "addtocart";
                    dataContent += item.ItemID;
                    dataContent += "\" data-title=";
                    dataContent += itemName;
                    dataContent += " data-onclick='AspxCommerce.RootFunction.AddToCartFromJS(";
                    dataContent += item.ItemID;
                    dataContent += ",";
                    dataContent += itemPriceValue;
                    dataContent += ",";
                    dataContent += itemSKU;
                    dataContent += ",";
                    dataContent += 1;
                    dataContent += ',"';
                    dataContent += item.IsCostVariantItem;
                    dataContent += "\",this);'";
                    RecentItemContents += "<div class=\"clearfix\">";
                    if (p.allowAddToCart.toLowerCase() == "true") {
                        if (p.allowOutStockPurchase.toLowerCase() == "false") {
                            if (item.IsOutOfStock) {

                                RecentItemContents += "<div class=\"cssClassAddtoCard\"><div class=\"sfButtonwrapper cssClassOutOfStock\"  data-ItemTypeID=\"";
                                RecentItemContents += item.ItemTypeID;
                                RecentItemContents += "\" data-ItemID=\"";
                                RecentItemContents += item.ItemID;
                                RecentItemContents += "\" ";
                                RecentItemContents += dataContent;
                                RecentItemContents += ">";
                                RecentItemContents += "<button type=\"button\"><span>";
                                RecentItemContents += getLocale(AspxCategoryWiseItem, "Out Of Stock");
                                RecentItemContents += "</span></button></div></div>";
                            }
                            else {
                                RecentItemContents += "<div class=\"cssClassAddtoCard\"><div data-ItemTypeID=\"";
                                RecentItemContents += item.ItemTypeID;
                                RecentItemContents += "\" data-ItemID=\"";
                                RecentItemContents += item.ItemID;
                                RecentItemContents += "\" ";
                                RecentItemContents += dataContent;
                                RecentItemContents += " class=\"sfButtonwrapper\">";
                                RecentItemContents += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button type=\"button\" class=\"addtoCart\"";
                                RecentItemContents += " addtocart=\"";
                                RecentItemContents += "addtocart";
                                RecentItemContents += item.ItemID;
                                RecentItemContents += "\" title=";
                                RecentItemContents += itemName;
                                RecentItemContents += " onclick='AspxCommerce.RootFunction.AddToCartFromJS(";
                                RecentItemContents += item.ItemID;
                                RecentItemContents += ",";
                                RecentItemContents += itemPriceValue;
                                RecentItemContents += ",";
                                RecentItemContents += itemSKU;
                                RecentItemContents += ",";
                                RecentItemContents += 1;
                                RecentItemContents += ',"';
                                RecentItemContents += item.IsCostVariantItem;
                                RecentItemContents += "\",this);' >";
                                RecentItemContents += getLocale(AspxCategoryWiseItem, "Cart +");
                                RecentItemContents += "</button></label></div></div>";
                            }
                        }
                        else {
                            RecentItemContents += "<div class=\"cssClassAddtoCard\"><div data-ItemTypeID=\"";
                            RecentItemContents += item.ItemTypeID;
                            RecentItemContents += "\" data-ItemID=\"";
                            RecentItemContents += item.ItemID;
                            RecentItemContents += "\"";

                            RecentItemContents += dataContent;
                            RecentItemContents += " class=\"sfButtonwrapper\">";
                            RecentItemContents += "<label class='i-cart cssClassCartLabel cssClassGreenBtn'><button type=\"button\" class=\"addtoCart\"";
                            RecentItemContents += " addtocart=\"";
                            RecentItemContents += "addtocart";
                            RecentItemContents += item.ItemID;
                            RecentItemContents += "\" title=";
                            RecentItemContents += itemName;
                            RecentItemContents += " onclick='AspxCommerce.RootFunction.AddToCartFromJS(";
                            RecentItemContents += item.ItemID;
                            RecentItemContents += ",";
                            RecentItemContents += itemPriceValue;
                            RecentItemContents += ",";
                            RecentItemContents += itemSKU;
                            RecentItemContents += ",";
                            RecentItemContents += 1;
                            RecentItemContents += ',"';
                            RecentItemContents += item.IsCostVariantItem;
                            RecentItemContents += "\",this);' >";
                            RecentItemContents += getLocale(AspxCategoryWiseItem, "Cart +");
                            RecentItemContents += "</button></label></div></div>";

                        }
                    }
                    if (aspxCommonObj().CustomerID > 0 && aspxCommonObj().UserName.toLowerCase() != "anonymoususer") {
                        RecentItemContents += "<div class=\"cssClassWishListButton\">";
                        RecentItemContents += "<label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=AspxCommerce.RootFunction.CheckWishListUniqueness(";
                        RecentItemContents += item.ItemID;
                        RecentItemContents += ",'";
                        RecentItemContents += item.SKU;
                        RecentItemContents += "',this);><span>";
                        RecentItemContents += getLocale(AspxCategoryWiseItem, "Wishlist+");
                        RecentItemContents += "</span></button></label></div>";
                    }
                    else {
                        RecentItemContents += "<div class=\"cssClassWishListButton\">";
                        RecentItemContents += "<label class='i-wishlist cssWishListLabel cssClassDarkBtn'><button type=\"button\" id=\"addWishList\" onclick=\"AspxCommerce.RootFunction.Login();\">";
                        RecentItemContents += "<span>";
                        RecentItemContents += getLocale(AspxCategoryWiseItem, "Wishlist+");
                        RecentItemContents += "</span></button></label></div>";
                    }

                    RecentItemContents += "</div></div>";
                    RecentItemContents += "</div></div>";
                    RecentItemContents += "</div>";


                    if (item.ItemRowNum <= p.noOfItemsInCategory) {
                        $('#div_' + item.CategoryID + '').append(RecentItemContents);
                    } else {
                        $('#divViewMore_' + item.CategoryID + '').html('');
                        var viewMore = '';
                        viewMore += '<a href="' + aspxRedirectPath + 'category/' + fixedEncodeURIComponent(item.CategoryName) + pageExtension + '">' + getLocale(AspxCategoryWiseItem, "View More") + '</a>';
                        $('#divViewMore_' + item.CategoryID + '').append(viewMore);
                    }
                    categoryWiseItemList.BindCurrencySymbol();
                
                },
                init: function () {
                    //categoryWiseItemList.GetCategoryWiseItemList(1, $('#ddlPageSize').val(), 0);
                    var items_per_page = 5;
                    if (p.rowTotal > 0) {
                        $("#divSearchPageNumber").show();
                    }
                    $('#Pagination').pagination(p.rowTotal, {
                        //  callback:'',
                        items_per_page: items_per_page,
                        //num_display_entries: 10,
                        current_page: currentPage,
                        callfunction: true,
                        function_name: { name: categoryWiseItemList.CategoryItemsList, limit: $('#ddlPageSize').val() },
                        prev_text: getLocale(AspxCategoryWiseItem, "Prev"),
                        next_text: getLocale(AspxCategoryWiseItem, "Next"),
                        prev_show_always: false,
                        next_show_always: false
                    });
                    //    $('.cssClassFormatCurrency').formatCurrency({ colorize: true, region: '' + region + '' });
                    $('#divitemImage a img[title]').tipsy({ gravity: 'n' });
                    $("#ddlPageSize").bind("change", function () {
                        var items_per_page = $(this).val();
                        var offset = 1;
                        categoryWiseItemList.CategoryItemsList(offset, items_per_page, 0);
                        $('#Pagination').pagination(p.rowTotal, {
                            //  callback:'',
                            items_per_page: items_per_page,
                            //num_display_entries: 10,
                            current_page: currentPage,
                            callfunction: true,
                            function_name: { name: categoryWiseItemList.CategoryItemsList, limit: $('#ddlPageSize').val() },
                            prev_text: getLocale(AspxCategoryWiseItem, "Prev"),
                            next_text: getLocale(AspxCategoryWiseItem, "Next"),
                            prev_show_always: false,
                            next_show_always: false
                        });
                        //$('.cssClassFormatCurrency').formatCurrency({ colorize: true, region: '' + region + '' });
                    });
                }
            };
            categoryWiseItemList.init();
        };
        $.fn.CategoryWiseItem = function (p) {
            $.CategoryWiseItemView(p);
        };
    })(jQuery);
    $(function () {
        $(".sfLocale").localize({
            moduleKey: AspxCategoryWiseItem
        });
        $(this).CategoryWiseItem({
            countryName: '<%=CountryName %>',
            defaultImagePath: '<%=DefaultImagePath %>',
            allowAddToCart: '<%=AllowAddToCart %>',
            allowOutStockPurchase: '<%=AllowOutStockPurchase %>',
            catWiseItemModulePath: '<%=CatWiseItemModulePath %>',
            noOfItemsInCategory: '<%=NoOfItemsInCategory %>',
            rowTotal: '<%=RowTotal %>',
            noOfItemsDisplayInARow: '<%=NoOfItemsDisplayInARow %>'
        });
    });
</script>
