﻿(function ($) {
    $.LatestItemOptionSettingView = function (param) {
        param = $.extend
    ({
        Settings: ''
    }, param);
        var p = $.parseJSON(param.Settings);
        function aspxCommonObj() {
            var aspxCommonInfo = {
                StoreID: AspxCommerce.utils.GetStoreID(),
                PortalID: AspxCommerce.utils.GetPortalID(),
                CultureName: AspxCommerce.utils.GetCultureName()
            };
            return aspxCommonInfo;
        }
        LatestItemOptionSetting = {
            config: {
                isPostBack: false,
                async: false,
                cache: false,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{}',
                dataType: 'json',
                baseURL: p.ModuleServicePath + "LatestItemsOptionsHandler.ashx/",
                method: "",
                url: "",
                ajaxCallMode: ""
            },
            ajaxCall: function (config) {
                $.ajax({
                    type: LatestItemOptionSetting.config.type,
                    contentType: LatestItemOptionSetting.config.contentType,
                    cache: LatestItemOptionSetting.config.cache,
                    async: LatestItemOptionSetting.config.async,
                    url: LatestItemOptionSetting.config.url,
                    data: LatestItemOptionSetting.config.data,
                    dataType: LatestItemOptionSetting.config.dataType,
                    success: LatestItemOptionSetting.config.ajaxCallMode,
                    error: LatestItemOptionSetting.config.ajaxFailure
                });
            },
            BindLatestItemOptionlSetting: function () {
                $("#txtLatestItemOptionCount").val(p.LatestItemOptionCount);
                $("#chkEnableLatestItemOptionRss").prop("checked", p.EnableLatestItemOptionRss);
                $("#txtLatestItemOptionRssCount").val(p.LatestItemOptionRssCount);
                $("#txtLatestItemOptionRssPage").val(p.LatestItemOptionRssPage);
            },
            LatestItemOptionSettingUpdate: function () {
                var latestItemOptionlCount = $.trim($("#txtLatestItemOptionCount").val());
                var enableLatestItemOptionRss = $("#chkEnableLatestItemOptionRss").prop("checked");
                var latestItemOptionRssCount = $.trim($("#txtLatestItemOptionRssCount").val());
                var latestItemOptionRssPage = $.trim($("#txtLatestItemOptionRssPage").val());
                var settingKeys = "LatestItemsOptionCount*EnableLatestItemsOptionRss*LatestItemsOptionRssCount*LatestItemsOptionRssPage";
                var settingValues = latestItemOptionlCount + '*' + enableLatestItemOptionRss + '*' + latestItemOptionRssCount + '*' + latestItemOptionRssPage;
                var param = JSON2.stringify({ SettingValues: settingValues, SettingKeys: settingKeys, aspxCommonObj: aspxCommonObj() });
                this.config.method = "LatestItemOptionSettingUpdate";
                this.config.url = this.config.baseURL + this.config.method;
                this.config.data = param;
                this.config.ajaxCallMode = LatestItemOptionSetting.LatestItemOptionSettingSuccess;
                this.ajaxCall(this.config);
            },
            LatestItemOptionSettingSuccess: function () {
                SageFrame.messaging.show(getLocale(LatestItemWithOptions, "Setting Saved Successfully"), "Success");
            },
            init: function () {
                LatestItemOptionSetting.BindLatestItemOptionlSetting();;
                $("#btnLatestItemOptionSettingSave").click(function () {
                    if (v.form()) {
                        LatestItemOptionSetting.LatestItemOptionSettingUpdate();
                    } else {
                        SageFrame.messaging.show(getLocale(CoreJsLanguage, "Please fill all the required form."), "Alert");
                    }
                });

                var v = $('#form1').validate({
                    messages: {
                        LatestItemOptionCount: {
                            required: '*'
                        },
                        LatestItemOptionRssCount: {
                            required: '*'
                        }
                    },
                    rules: {
                        LatestItemOptionCount: {
                            digits: true
                        },
                        LatestItemOptionRssCount: {
                            digits: true
                        },
                    }
                });
            }
        };
        LatestItemOptionSetting.init();
    };
    $.fn.LatestItemOptionSetting = function (p) {
        $.LatestItemOptionSettingView(p);
    };
})(jQuery);