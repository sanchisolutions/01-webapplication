﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanchiCommerce.Core
{
   public class PriceHistoryController
    {
       public static List<PriceHistoryInfo> GetPriceHistory(int itemId,AspxCommonInfo SanchiCommerceObj)
       {
           List<PriceHistoryInfo> list = PriceHistoryPrivider.GetPriceHistory(itemId, SanchiCommerceObj);
           return list;
       }
       public static List<PriceHistoryInfo> BindPriceHistory(int offset,int limit, AspxCommonInfo SanchiCommerceObj,string itemName,string userName)
       {
           List<PriceHistoryInfo> list = PriceHistoryPrivider.BindPriceHistory(offset, limit, SanchiCommerceObj,itemName,userName);
           return list;
       } 
    }
}
